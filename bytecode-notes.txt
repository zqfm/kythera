; +--------+--------+--------------------+ ;
; | 0x0A00 | [9][0] | Sustenance Potion  | ;
; +--------+--------+--------------------+ ;

86 ; set-field
  28 ; nutrition
  30 ; arg00
  40 ; end

  41 ; byte
    18 ; 0x18
40 ; end

;;; arg00.nutrition = 24

8D ; if-not
  30 ; arg00
  48 ; global
    05 ; player character
  54 ; equal
40 ; end

00 40 ; goto 0040

;;; if arg00 != global5 then goto end

59 6F 75 20 66 65 65 6C 20 66 75 6C 6C 79 20 73 61 74 65 ; You feel fully sate
64 2C 20 68 75 6E 67 65 72 20 61 6E 64 20 74 68 69 72 73 ; d, hunger and thirs
74 20 67 6F 6E 65 2E 0A                                  ; t gone.\n

8B ; return
  41 ; byte
    00 ; 0x00
40 ; end

; +--------+--------+----------------+ ;
; | 0x0A01 | [9][1] | Healing Potion | ;
; +--------+--------+----------------+ ;

86 ; set-field
  1C ; health
  30 ; arg00
  40 ; end

  30 ; arg00
  62 ; get-field
    1C ; health
  41 ; byte
    0A ; 0x0a
  4A ; add

;; offset 13
  AC ; sys random
    41 ; byte
      01 ; 0x01
    41 ; byte
      0A ; 0x0a
  40 ; end

  4A ; add
40 ; end

8D ; if-not
  30 ; arg00
  48 ; global
    05 ; player character
  54 ; equal
40 ; end

00 33 ; goto 0033

54 61 73 74 79 20 61 6E 64 20 72 65 66 72 65 73 68 69 6E ; Tasty and refreshin
67 2E 0A                                                 ; g.\n

8B ; return
  41 ; byte
    00 ; 0x00
40 ; end

; +--------+--------+----------------------+ ;
; | 0x0A02 | [9][2] | Mage's Friend Potion | ;
; +--------+--------+----------------------+ ;
; local00 = 10 + random(1,10)
;
; // if already at full magic, do nothing
; if arg00.magic == arg00.full-magic {
;   if arg00 == globals.player-charcter {
;     "Nothing seems to happen."
;   }
;   return 0
; }
;
; // cap magic at full-magic
; if arg00.full-magic - arg00.magic < local00 {
;   local00 = arg00.full-magic - arg00.magic
; }
;
; // don't kill the character
; if local00 > arg00.health {
;   local00 = arg00.health - 1
; }
;
; if local00 > 0 then {
;   if arg00 == player-character then {
;     "You experience a brief pain and weakness.\n"
;   }
;   arg00.magic = arg00.magic + local00
;   arg00.health = arg00.heath - local00
;   return 0
; }
; "Nothing seems to happen.\n"
; return 0


82 ; set local
  00 ; local00
  41 ; byte
    0A ; 10
  AC ; sys random
    41 ; byte
      01 ; 0x01
    41 ; byte
      0A ; 0x0a
  40 ; end
  4A ; add
40 ; end

;;; local00 = 10 + random(1,10)

8D ; if-not
  30 ; arg00
  62 ; get-field
    1E ; magic
  30 ; arg00
  62 ; get-field
    1F ; full-magic
  54 ; eq
40 ; end

00 3D ; goto 003d

;;; if arg00.magic != arg00.full-magic then goto 003d

8D ; if-not
  30 ; arg00
  48 ; global
    05 ; player-character
  54 ; eq
40 ; end

00 3A ; goto 003a

;;; if arg00 != globals.player-charcter then goto 003a

4E 6F 74 68 69 6E 67 20 73 65 65 6D 73 20 74 6F 20 68 61 ; Nothing seems to ha
70 70 65 6E 2E                                           ; ppen.

;;; offset 003a
88 ; goto
  00 D8 ; 00d8

;;; offset 003d
8D ; if-not
  30 ; arg00
  62 ; get-field
    1F ; full-magic
  30 ; arg00
  62 ; get-field
    1E ; magic
  4B ; sub
  00 ; local00
  4F ; lt
40 ; end

00 54 ; goto 0054

;;; if arg00.full-magic - arg00.magic >= local00 then goto 0054

82 ; set-local
  00 ; local00
  30 ; arg00
  62 ; get-field
    1F ; full-magic
  30 ; arg00
  62 ; get-field
    1E ; magic
  4B ; sub
40 ; end

;;; local00 = arg00.full-magic - arg00.magic

;;; offset 0054
8D ; if-not
  00 ; local00
  30 ; arg00
  62 ; get-field
    1C ; health
  51 ; gt
40 ; end

00 66 ; goto 0066

;;; if-not local00 > arg00.health then goto 0066

82 ; set-local
  00 ; local00
  30 ; arg00
  62 ; get-field
    1C ; health
  41 ; byte
    01 ; 0x01
  4B ; sub
40 ; end

;;; local00 = arg00.health - 1

;;; offset 0066
8D ; if-not
  00 ; local00
  41 ; byte
    00 ; 00
  51 ; gt
40 ; end

00 B7 ; goto 00b7

;;; if-not local00 > 0 then goto 00b7

8D ; if-not
  30 ; arg00
  48 ; global
    05 ; player-character
  54 ; eq
40 ; end

00 A0 ; goto 00a0

;;; if arg00 != player-character then goto 00a0

59 6F 75 20 65 78 70 65 72 69 65 6E 63 65 20 61 20 62 72 ; You experience a br
69 65 66 20 70 61 69 6E 20 61 6E 64 20 77 65 61 6B 6E 65 ; ief pain and weakne
73 73 2E 0A                                              ; ss.\n

;;; offset 00a0
86 ; set-field
  1E ; magic
  30 ; arg00
  40 ; end

  30 ; arg00
  62 ; get-field
    1E ; magic
  00 ; local00
  4A ; add
40 ; end

;;; arg00.magic = arg00.magic + local00

86 ; set-field
  1C ; health
  30 ; arg00
  40 ; end

  30 ; arg00
  62 ; get-field
    1C ; health
  00 ; local00
  4B ; sub
40 ; end

;;; arg00.health = arg00.heath - local00

88 ; goto
  00 D8 ; 00d8

;;; goto 00d8

;;; offset 00b7
8D ; if-not
  30 ; arg00
  48 ; global
    05 ; player-character
  54 ; eq
40 ; end

00 D8 ; goto 00d8

;; if arg00 != player-character then goto 00d8

4E 6F 74 68 69 6E 67 20 73 65 65 6D 73 20 74 6F 20 68 61 ; Nothing seems to ha
70 70 65 6E 2E 0A                                        ; ppen.\n

;;; offset 00d8
8B ; return
  41 ; byte
    00 ; 0x00
40 ; end

;;; return 0

; +--------+--------+--------------------+ ;
; | 0x0A03 | [9][3] | Free Motion Potion | ;
; +--------+--------+--------------------+ ;

c2 ; sys clear-flag
  30 ; arg00
  41 ; byte
    16 ; 0x16
40 ; end

c2 ; sys clear-flag
  30 ; arg00
  41 ; byte
    0e ; 0x0e
40 ; end

8d ; if-not
  30 ; arg00
  48 ; global
    05 ; player-character
  54 ; eq
40 ; end

00 3d ; goto 003d

59 6f 75 72 20 61 72 65 20 77 69 64 65 20 61 77 61 6b 65
2c 20 65 76 65 72 79 20 6e 65 72 76 65 20 61 77 61 72 65
2e 0a

8b ; return
  41 ; byte
    00 ; 0x00
40 ; end

; +--------+--------+-----+ ;
; | 0x0901 | [8][1] | ??? | ;
; +--------+--------+-----+ ;

8D ; if-not
  F5 ; sys get-skill
    30 ; arg00
    32 ; arg02
  40 ; end
40 ; end

00 12 ; goto 0012

8B ; return
  43 ; word
    50 00 00 01 ; true
40 ; end

8D ; if-not
  30 ; arg00
  60 ; has-field
    44 ; 0x44
40 ; end

00 23 ; goto 0023

82 ; set-local
  00 ; local00
  30 ; arg00
  61 ; class-field
    44 ; 0x44
    00 ; 0x00
40 ; end

88 ; goto
  00 37 ; 0037

82 ; set-local
  01 ; local01
  30 ; arg0
  63 ; cast
    48 ; monster
40 ; end

8D ; if-not
  01 ; local01
  60 ; has-field
    44 ; 0x44
40 ; end

00 37 ; goto 0037

82 ; set-local
  00 ; local00
  01 ; local01
  61 ; class-field
    44 ; 0x44
    00 ; 0x00
40 ; end

82 ; set-local
  02 ; local02
  43 ; word
    50 00 00 01 ; true
40 ; end

82 ; set-local
  03 ; local03
  A1 ; sys array-iterator
    43 ; word
      10 00 00 04 ;
    41 ; byte
      00 ; 0x00
    00 ; local00
  40 ; end
40 ; end

8C ; if
  A1 ; sys array-iterator
    43 ; word
      10 00 00 04 ;
    41 ; byte
      01 ; 0x01
  40 ; end
40 ; end

00 8E ; goto 008e

8D ; if-not
  02 ; local02
40 ; end

00 69 ; goto 0069

82 ; set-local
  02 ; local02
  43 ; word
    50 00 00 00 ; false
40 ; end

88 ; goto
  00 7F ; 007f

8D ; if-not
  03 ; local03
  32 ; arg03
  54 ; eq
40 ; end

00 77 ; goto 0077

8B ; return
  43 ; word
    50 00 00 01 ; true
40 ; end

82 ; set-local
  02 ; local02
  43 ; word
    50 00 00 01 ; true
40 ; end

82 ; set-local
  03 ; local03
  A1 ; sys array-iterator
    43 ; word
      10 00 00 04
    41 ; byte
      02 ; 0x02
  40 ; end
40 ; end

88 ; goto
  00 4C ; 004c

8B ; return
  43 ; word
    50 00 00 00 ; false
40 ; end

8B ; return
  41 ; byte
    00 ; 0x00
40 ; end

; +--------+----------+-----+ ;
; | 0x0981 | [8][129] | ??? | ;
; +--------+----------+-----+ ;


8D ; if-not
  9F ; call-resource
    09 01 ; 0901
    30 ; arg00
    31 ; arg01
    32 ; arg02
  40 ; end
40 ; end

00 76 ; goto 0076

82 00 AD 41 1C 41 00 30 41 00 32 41 00 41 00 40 63 50 40 82 01 00 61 36 00 40 8D 01 41 03 54 01 41 05 54 5D 40 00 42 F0 30 41 4B 32 41 00 41 00 40 88 00 73 8D 01 41 01 54 40 00 6A 8D 9F 0E 8C 30 31 40 41 02 51 40 00 5E A7 00 40 8B 41 00 40 F0 30 41 4C 32 41 00 31 40 88 00 73 F0 30 41 4C 32 41 00 31 40 A7 00 40 8B 41 00 40

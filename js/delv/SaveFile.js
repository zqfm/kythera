import DataFile from './DataFile.js';


export default class SaveFile extends DataFile {

  constructor (file) {
    super( file );
  }

  get player_name () {
    return this.getPascalString( 0x20 );
  }

}

import Resource from './Resource.js';
import Tile from './Tile.js';

export default class Prop {

  constructor (src, opts={}) {
    this.src = src;

    Object.assign( this, opts );

    this._offset = this._offset || 0;

    this.tile_id = opts.tile_id ? opts.tile_id
    // if flags is 0x42, then this is an egg
      : this.flags === 0x42 ? 0x16c
      : src.prop_tiles[ this.prop_id ];

    if (this.flags === 0x42 && this.aspect === 10) {
      // aspect 10 is used to draw the mystical ribbons in the ethereal void
      this.draw = (ctx, x, y) => {
        src.data[142][this.prop_id].draw( ctx, x, y, );
      };
    }

    this.setExtension();

  }

  setExtension (aspect=this.aspect) {
    // if a prop occupies multiple tiles, keep track of additional tiles via "extended" props
    const extend = extension => {
      return new Prop( this.src, { prop_id: this.prop_id, _aspect: this.aspect, extension, prop: this, _offset: this._offset, rotate: this.rotate } );
    };
    const flag = this.attributes & 0xc0;
    this.extension_n = null;
    this.extension_w = null;
    this.extension_nw = null;
    if (this.rotate) {
      if (flag === 0x40) {
        this.extension_w = extend( -1 );
      } else if (flag === 0x80) {
        this.extension_n = extend( -1 );
      } else if (flag === 0xc0) {
        this.extension_nw = extend( -3 );
        this.extension_w = extend( -2 );
        this.extension_n = extend( -1 );
      }
    } else {
      if (flag === 0x40) {
        this.extension_n = extend( -1 );
      } else if (flag === 0x80) {
        this.extension_w = extend( -1 );
      } else if (flag === 0xc0) {
        this.extension_nw = extend( -3 );
        this.extension_n = extend( -2 );
        this.extension_w = extend( -1 );
      }
    }
  }

  get aspect () {
    if (this.prop) {
      return this.prop.aspect + this.extension;
    } else {
      return this._aspect;
    }
  }

  set aspect (aspect) {
    if (this.prop) {
      this.prop.aspect = aspect;
    } else {
      this._aspect = aspect;
      this.setExtension( aspect );
    }
  }

  get x () { return this._x; }
  get y () { return this._y; }

  set x (x) {
    if (this.zone) {
      this.zone.movePropTo( this, x, this._y );
    } else {
      this._x = x;
    }
  }

  set y (y) {
    if (this.zone) {
      this.zone.movePropTo( this, this._x, y );
    } else {
      this._y = y;
    }
  }

  get offset () {
    return [ this.src.xoffsets[ (this.prop_id << 5) + this.aspect ] + this._offset,
             this.src.yoffsets[ (this.prop_id << 5) + this.aspect ] + this._offset ];
  }

  get name () {
    const name = this.src.tile_names[ this.tile_id + this.aspect ];
    if (this.flags === 0x42) {
      switch (this.aspect) {
      case 3: return 'SOUND';
      case 5: return 'MUSIC';
      case 8: return 'ROOM';
      case 0:
      default: return 'EGG';
      }
    }
    return this.flags === 0x44 ? 'ROOF'
      : this.flags & 0xe0 ? `${name}?`
      : name;
  }

  get attributes () {
    return this.src.tile_attributes[ this.tile_id + this.aspect ];
  }

  get script_class () {
    return this.src.getResource( 0x1000 | this.prop_id );
  }

  /**
   * What container this item is inside
   **/
  get container () {
    return (this.raw_location & 0xffff) - 0x100;
  }

  /**
   * Truthy if this prop is inside a container
   **/
  get contained () {
    return this.flags & 0x18;
  }

  /**
   * Whether or not to draw this item on the map
   **/
  get visible () {
    // TODO this is probably wrong
    return this.flags !== 0xff
      && !(this.flags & 0x58);
  }

  /**
   * Whether or not the item is owned by someone (eg. taking it is theft)
   **/
  get owned () {
    return this.flags & 0x1;
  }

  get d1 () {
    return this.d3 >> 8;
  }

  set d1 (d1) {
    this.d3 = (this.d3 & 0xff) | (d1 << 8);
  }

  get d2 () {
    return this.d3 & 0xff;
  }

  set d2 (d2) {
    this.d3 = (this.d3 & 0xff00) | d2;
  }

  draw (ctx, x, y, { aspect=this.aspect, rotate=this.rotate, offset=this.offset }={}) {
    const tile_id = this.tile_id + aspect;

    this.src.tilesheets[ (tile_id >>> 4) & 0xff ]
      .drawTile( ctx, tile_id & 0xf, x, y, { rotate, offset, mask: true } );

  }

}

Prop.load = (src, byteOffset=0, opts={}) => {

  const res = new Resource( src, byteOffset, 16, opts );
  const flags = res.getUint8(0);
  const [_x,_y] = res.getPair12(1);

  const u16 = res.getUint16(4);
  const prop_id = u16 & 0x3ff;
  const aspect = (u16 >>> 10) & 0x1f;
  const u = res.getUint16(14);

  return new Prop( src, {
    prop_id,
    aspect,
    resource: res,
    flags,
    raw_location: res.getUint24(1),
    _x,
    _y,
    rotate: (u16 >>> 10) & 0xe0,

    // d3 is quantity for things like oboloi or torches
    // and it is the room bounds for room eggs
    // and it may be the portal entry for zone exit points
    d3: res.getUint16(6),
    propref: res.getUint16(8),
    storeref: res.getUint32(10),
    u,
    zone: opts.zone,
    index: opts.index,
    scripts: opts.scripts,
    _offset: (u & 0x3000)
      ? 0
      : (u >>> 6) & 0x1f, // guess!
  } );

};

Prop.fromFauxProp = (src, word) => {
  return new Prop( src, {
    prop_id: word & 0x3ff,
    aspect: (word >>> 10) & 0x1f,
    rotate: word >>> 15,
    faux_prop: true } );
};

import Resource, { hex } from './Resource.js';
import Prop from './Prop.js';
import Zone from './Zone.js';
import Character from './Character.js';
import Monster from './Monster.js';
import Skill from './Skill.js';
import Room from './Room.js';
import randomId from '../random-id.js';
import { GLOBAL, OP_END } from './constants.js';

const nested = true; // for shorter keys later

export default function makeScript (src, opts={}) {
  const res = opts.resource || new Resource( src, opts.res_offset, opts.res_size, opts );
  const offset = opts.offset || 0;
  const type = res.getUint8( offset );

  if (type === 0x81) {
    return new ScriptFunction( src, res, opts );
  } else if ((type & 0xf0) === 0x90) {
    return new ScriptArray( src, res, { ...opts, override_dref: res.resid === 0x201 ? 0x201 : null } );
  } else if ((type & 0xf0) === 0xa0) {
    return new ScriptTable( src, res, opts );
  } else if (type < 0x80 && opts.nested) {
    return res.getCString( offset );
  } else if (!opts.nested && res.getUint16(1)) {
    return new ScriptClass( src, res, opts );
  } else {
    return res;
  }

}

function repr (v) {
  return typeof v === 'string' ? `"${v}"` : v;
}

function tos (body,i,ary) {
  if (body instanceof Array) {
    return `(${body.map( tos ).join(' ')})`;
  } else if (body && body.name) {
    return body.name;
  } else if (typeof body === 'string' && i > (typeof ary[0] === 'number' ? 1 : 0)) {
    return '"' + body + '"';
  } else {
    return String(body);
  }
};

function range(n) {
  return [...Array(n).keys()];
}

function word_val (word, ctx) {
  let type = 'word';
  if (word === 0x50000000) {
    type = 'bool';
    word = false;
  } else if (word === 0x50000001) {
    type = 'bool';
    word = true;
  } else if (word === 0x5000ffff) {
    type = 'nil';
    word = null;
  } else if (word === 0x5000fffe) {
    type = 'empty';
    word = null;
  } else if (0x10000000 <= word && word <= 0x10000030) {
    type = 'ref';
    word = (word&0xff)-1;
    if (!ctx) { word = var_name( word ); }
  } else if (0x20000000 <= word && word < 0x30000000) {
    // global?
    type = 'global';
    word &= 0xffff;
    if (ctx) {
      word = ctx.globals[ word ];
    } else {
      word = globals[ word ];
    }
  } else if (0x30000000 <= word && word < 0x40000000) {
    type = 'res';
    word &= 0xffff;
    if (ctx) {
      word = ctx.src.getResource( word );
    }
  } else if (0x40000000 <= word && word < 0x50000000) {
    console.assert( (0xff000000 & word) === 0x40000000 );
    const klass = casts[ (0xff0000 & word) >> 16 ];
    const which = 0xffff & word;
    if (klass) {
      type = klass.name;
      if (ctx) {
        if (klass.get) {
          word = klass.get( ctx, which );
        } else {
          console.warn( `unimplemented ${klass.name}.get`, which );
          word = which;
        }
      } else {
        word = which;
      }
    }
  } else if (0x8000000 < word && word <= 0xfffffff) {
    word |= ~(-1&0xfffffff);
  }
  return [type,word];
}

/**
 * Copy of ASM_STRUCT_HINTS from delvmod/rdasm_symbolics.py
 **/
const fields = {
  [0x00]: 'flags',
  [0x01]: 'x',
  [0x02]: 'y',
  [0x03]: 'aspect',
  [0x04]: 'obj_type',
  [0x05]: 'prop_id',
  [0x06]: 'd1',
  [0x07]: 'd2',
  [0x08]: 'd3',
  [0x09]: 'quantity',
  // aspect lower 4 bits
  [0x0A]: 'tile',
  [0x0B]: 'container',
  // as polyps or slimes
  [0x0C]: 'erode_effect',
  [0x0D]: 'rotate',
  // 0x0E-0x10 unseen
  [0x11]: 'has_storage',
  [0x12]: 'storage',
  [0x13]: 'bit_flags',

  // status_flags & 1 = alive
  [0x14]: 'status_flags',

  // behavioral mode
  // 6 might be attacking
  // 0x0f might be patrolling horizontally
  // 0x10 might be patrolling vertically
  // 0x86-0x89 = 134-137 = standing still
  // 0x90 = 144 = working
  // 0x91 = 145 = sleeping
  // 0x92 = 146 = sitting
  // 0x93 = 147 = eating
  // 0x94 = 148 = farming
  [0x15]: 'behavior',
  // 0x16 appears to get the same value
  [0x16]: 'behavior2',
  // 0x17-0x19, cast to character before using
  [0x17]: 'body',
  [0x18]: 'reflex',
  [0x19]: 'mind',
  [0x1A]: 'exp',
  [0x1B]: 'level',
  [0x1C]: 'health',
  [0x1D]: 'full_health',
  [0x1E]: 'mana',
  [0x1F]: 'full_mana',
  [0x20]: 'archetype',
  [0x21]: 'training',
  [0x22]: 'target',
  // semantics unsure
  [0x23]: 'timing',
  // 0x24-0x25 - haven't been able to figure them out, something to do with storing the prop when the person sleeps?
  // on new character creation, 0x24 and 0x25 are set to 33 when female, 32 when male
  [0x26]: 'talk_balloon',
  [0x28]: 'nutrition',
  // Seems to be this from context, doesn't work
  [0x29]: 'room_occupied',
  // seen in monster.
  [0x32]: 'monster_flags', // 0x4000: bleeds when hit
  [0x35]: 'alignment',
};

/**
 * Copy of DASM_OBJ_NAME_HINTS
 **/
const casts = {
   [0x00]: Prop,
   [0x20]: Zone,
   [0x40]: Character,
   [0x48]: Monster,
   [0x50]: Skill,
   [0x58]: Room,
};

const types = {
  // created in conversation with alaric with arguments
  // 0, player_character, (resource(0x501)[i] >> 10) | 0x10, resource(0x501)[i] & 0x3ff, 0, 0
  [0x1c]: Skill,

  // created in conversation with magpie with arguments
  // 0, 1, 9, 0, 2, 0
  [0x42]: null,
};

/**
 * Copy of DASM_GLOBAL_NAME_HINTS
 **/
const globals = {
  [0x00]: 'current_hour',
  [0x01]: 'current_time',
  [0x02]: 'player_character_name',
  [0x05]: 'player_character',
  [0x06]: 'characters_in_party',
  [0x07]: 'characters_in_party2',
  [0x09]: 'current_character',
  [0x0A]: 'conversation_response',
  [0x0C]: 'karma',
  [0x0D]: 'registered',
  [0x0E]: 'languages_known',
  [0x0F]: 'game_day',
  [0x10]: 'current_zone',
  [0x11]: 'difficulty_level',
  [0x12]: 'current_room',
  [0x13]: 'is_player_turn',
  [0x17]: 'difficulty',
};

function expect (expected, actual) {
  console.assert( expected === actual, `expected 0x${hex( expected )} but got 0x${hex( actual )}` );
}


function var_name (op) {
  if (0x00 <= op && op <= 0x2f) {
    return `local${hex(op)}`;
  } else if (0x30 <= op && op <= 0x3f) {
    return `arg${hex(op-0x30)}`;
  }
}

/**
 * Dummy no-op operation for erroneous or unimplemented ops
 **/
const noop = async (res,off,ctx) => {
  console.warn( 'noop at ' + off );
  return [ off+1, `nop${hex(res.getUint8(off))}` ];
};

/**
 * Reads a list of expressions until an 0x40 (end) is found
 **/
const expressions = async (res,off,ctx) => {
  let op = res.getUint8(off);
  const exprs = [];
  while (op != OP_END) {
    let data;
    [off,...data] = await sops[ op ]( res, off, ctx );
    exprs.push([ ...data ]);
    op = res.getUint8(off);
  }
  return [ off+1, ...exprs ];
};

const readToOp = (res,off) => {
  let step = 0;
  let str = '';
  while (true) {
    const op = res.getUint8(off);
    if (op >= 0x80) { break; }
    str += String.fromCharCode( op );
    if (++off >= res.byteLength) { break; }
  }
  return str;
};

const vari = (ctx,n) => {
  if (0x00 <= n && n <= 0x2f) {
    return ctx.locals[ n ];
  } else if (0x30 <= n && n <= 0x3f) {
    return ctx.args[ n - 0x30 ];
  } else {
    throw new Error('invalid variable 0x'+hex(n));
  }
};

/**
 * Table of opcodes
 * Each opcode is a function that, when given a resource and offset
 * returns the opcode size, the opcode mnemonic, and the opcode data, if any
 **/
const ops = new Proxy(
  {

    [0x81]: (res,off,ctx,end) => {
      const fn = makeScript( res.src, { resource: res, nested, size: end - off, offset: off } );
      if (ctx) {
        ctx.stack.push( fn );
      }
      return [off+fn.size,'function',fn];

    },

    [0x82]: async (res,off,ctx) => {
      const local = res.getUint8(off+1);
      let exprs;
      [off,...exprs] = await expressions( res, off+2, ctx );
      if (ctx) {
        const value = ctx.stack.pop();
        ctx.locals[ local ] = value;
      }
      return [off,'set-local',var_name(local),...exprs];
    },

    [0x83]: async (res,off,ctx) => {
      const offset = res.getUint16( off+1 );
      let exprs;
      [off, ...exprs] = await expressions( res, off+3, ctx );
      if (ctx) {
        const word = ctx.stack.pop();
        res.setUint32( offset, word );
      }
      return [off,'write-word-at',offset,...exprs];
    },

    [0x84]: async (res,off,ctx) => {
      let exprs0, exprs1, exprs2;
      [off,...exprs0] = await expressions( res, off+1, ctx );
      [off,...exprs1] = await expressions( res, off, ctx );
      [off,...exprs2] = await expressions( res, off, ctx );
      if (ctx) {
        const value = ctx.stack.pop();
        const idx = ctx.stack.pop();
        const array = ctx.stack.pop();
        array.set( idx, value );
      }
      return [ off, 'set-index', ...exprs0, ...exprs1, ...exprs2 ];
    },

    [0x85]: async (res,off,ctx) => {
      const resid = res.getUint16(off+1);
      const offset = res.getUint16(off+3);
      let exprs;
      [off, ...exprs] = await expressions( res, off+5, ctx );
      if (ctx) {
        const word = ctx.stack.pop();
        ctx.src.getResource( resid ).resource.setUint32( offset, word );
      }
      return [ off, 'write-word-to-resource', resid, offset, ...exprs ];
    },

    [0x86]: async (res,off,ctx) => {
      const fidx = res.getUint8(off+1);
      const field = fields[fidx] || fidx;
      let exprs0, exprs1;
      [off, ...exprs0] = await expressions( res, off+2, ctx );
      [off, ...exprs1] = await expressions( res, off, ctx );
      if (ctx) {
        const value = ctx.stack.pop();
        const obj = ctx.stack.pop();
        obj[ field ] = value;
      }
      return [ off, 'set-field', field, ...exprs0, ...exprs1 ];
    },

    [0x87]: async (res,off,ctx) => {
      const glob = res.getUint8(off+1);
      let exprs;
      [off, ...exprs] = await expressions( res, off+2, ctx );
      if (ctx) {
        ctx.globals[ glob ] = ctx.stack.pop();
      }
      return [ off, 'set-global', globals[glob]||glob, ...exprs ];
    },

    [0x88]: async (res,off,ctx) => {
      const goto = res.getUint16(off+1);
      if (ctx) {
        return [goto];
      }
      return [off+3,'goto',goto];
    },

    [0x89]: async (res,off,ctx) => {
      let exprs;
      [off, ...exprs] = await expressions( res, off+1, ctx );
      const ncases = res.getUint16( off );
      if (ctx) {
        const cond = ctx.stack.pop();
        console.assert( cond < ncases, `unhandled switch case ${cond} (max ${ncases}) at offset ${off}` );
        const goto = res.getUint16( off+2+(cond*2) );
        return [goto];
      }
      const cases = [];
      for (let i = 0; i < ncases; ++i) {
        cases.push( ['case',i,res.getUint16( off+2+(i*2) )] );
      }
      return [off+2+(ncases*2),'switch',exprs,cases];
    },

    [0x8a]: async (res,off,ctx) => {
      let exprs;
      [off, ...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        await ctx.emit('print', ctx.stack.pop());
      }
      return [off,'print',...exprs];
    },

    [0x8b]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const value = ctx.stack.pop();
        return [-1, 'return', value];
      }
      return [off,'return',...exprs];
    },

    [0x8c]: async (res,off,ctx) => {
      let cond;
      [off,...cond] = await expressions( res, off+1 , ctx );
      const goto = res.getUint16( off );
      if (ctx) {
        if (ctx.stack.pop()) {
          return [goto];
        } else {
          return [off+2];
        }
      }
      return [off+2, 'if', cond, ['goto',goto]];
    },

    [0x8d]: async (res,off,ctx) => {
      let cond;
      [off,...cond] = await expressions( res, off+1, ctx );
      const goto = res.getUint16( off );
      if (ctx) {
        if (ctx.stack.pop()) {
          return [off+2];
        } else {
          return [goto];
        }
      }
      return [off+2, 'if-not', cond, ['goto',goto]];
    },

    [0x8e]: async (res,off,ctx) => {
      if (ctx) {
        ctx.globals[GLOBAL.CONVERSATION_RESPONSE] = null;
        await ctx.emit( 'done-talking' );
      }
      return [off+1,'done-talking'];
    },

    [0x8f]: async (res,off,ctx) => {
      const str = res.getCString( off+1 );
      if (ctx) {
        ctx.emit('conversation-prompt',str);
      }
      return [off+str.length+2,'conversation-prompt',str];
    },

    [0x90]: async (res,off,ctx) => {
      const test = res.getCString( off+1 );
      const len = test.length;
      const goto = res.getUint16( off+len+2 );
      if (ctx) {
        let response = ctx.globals[GLOBAL.CONVERSATION_RESPONSE];
        if (!response && response !== '') {
          await ctx.emit( 'conversation-response' );
          response = ctx.globals[GLOBAL.CONVERSATION_RESPONSE];
        }
        if (test === '*' || test.split(',').some( test => (response || 'Bye').toLowerCase().match( new RegExp( '^' + test ) ) )) {
          ctx.globals.last_response = response;
          return [off+len+4];
        } else {
          return [goto];
        }
      }
      return [off+len+4,'conversation-response', ['string',test], ['goto',goto]];
    },

    [0x92]: async (res,off,ctx) => {
      const x = res.getUint8(off+1);
      let exprs;
      [off,...exprs] = await expressions( res, off+2, ctx );
      if (ctx) {
        console.warn( 'unimplemented ai-state', ...exprs );
      }
      return [off,'ai-state',x,...exprs];
    },

    [0x93]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        ctx.emit('gui-close', ctx.stack.pop());
      }
      return [off,'gui-close',...exprs];
    },

    [0x9b]: async (res,off,ctx) => {
      const op = res.getUint8( off+1 );
      let exprs;
      [off,...exprs] = await expressions( res, off+2, ctx );
      if (ctx) {
        // op = 1 may be to display a scrollbar
        ctx.emit('gui', { op, data: ctx.stack.pop() });
      }
      return [off, 'gui', op, ...exprs];
    },

    [0x9c]: async (res,off,ctx) => {
      const resid = res.getUint16( off+1 );
      let exprs0, exprs1;
      [off,...exprs0] = await expressions( res, off+3, ctx );
      [off,...exprs1] = await expressions( res, off, ctx );
      if (ctx) {
        console.warn( 'unimplemented call-index', exprs0, exprs1 );
      }
      return [off,'call-index',resid,...exprs0,...exprs1];
    },

    [0x9d]: async (res,off,ctx) => {
      const method_idx = res.getUint8( off+1 );
      let exprs;
      [off,...exprs] = await expressions( res, off+2, ctx );
      if (ctx) {
        let obj = ctx.stack.pop();
        if (obj instanceof Prop) {
          obj = ctx.src.getResource( 0x1000 | obj.prop_id );
        }
        const method = obj.table.get( method_idx );
        const args = [];
        for (let i = 0; i < method.param_count; ++i) {
          args.push( ctx.stack.pop() );
        }
        const ret = await method.call( ...args.reverse() );
        ctx.stack.push( ret );
      }
      return [ off, 'call-method', method_idx, ...exprs ];
    },

    [0x9e]: async (res,off,ctx) => {
      const offset = res.getUint16( off+1 );
      let exprs;
      [off,...exprs] = await expressions( res, off+3, ctx );
      if (ctx) {
        const obj = res.src.getResource( res.resid );
        const fn = obj.get( `sub${hex(offset,4)}` );
        const args = [];
        for (let i = 0; i < fn.param_count; ++i) {
          args.push( ctx.stack.pop() );
        }
        const ret = await fn.call( ...args.reverse() );
        ctx.stack.push( ret );
      }
      return [off,'call-function-at',offset,...exprs];
    },

    [0x9f]: async (res,off,ctx) => {
      const resid = res.getUint16( off+1 );
      let exprs;
      [off,...exprs] = await expressions( res, off+3, ctx );
      if (ctx) {
        const fn = ctx.src.getResource( resid );
        const args = [];
        for (let i = 0; i < fn.param_count; ++i) {
          args.push( ctx.stack.pop() );
        }
        ctx.stack.push( await fn.call( ...args.reverse() ) );
      }
      return [off,'call-resource',resid,...exprs];
    },

    [0xa0]: async (res,off,ctx) => {
      let exprs;
      if (ctx) {
        const len = ctx.stack.length;
        [off,...exprs] = await expressions( res, off+1, ctx );
        let end, start;
        if (ctx.stack.length - len === 4) {
          end = ctx.stack.pop();
          start = ctx.stack.pop();
        }
        const op = ctx.stack.pop();
        const ref = ctx.stack.pop();
        switch (op) {
        case 0: // init
          console.log( 'iterator init', start, end );
          ctx.iter.set( ref, { start, end } );
          ctx.stack.push( start );
          break;
        case 1: // done
          console.log( 'iterator done?', vari( ctx, ref ), ctx.iter.get( ref ).end );
          ctx.stack.push( vari( ctx, ref ) >= ctx.iter.get( ref ).end );
          break;
        case 2: // next
          console.log( 'iterator next', vari( ctx, ref ) + 1 );
          ctx.stack.push( vari( ctx, ref ) + 1 );
          break;
        default:
          throw new Error('invalid iterator op 0x' + hex(op));
        }
      } else {
        [off,...exprs] = await expressions( res, off+1, ctx );
      }
      return [off, 'range-iterator', ...exprs];
    },
    [0xa1]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented array-iterator', ...exprs );
      }
      return [off, 'sys', 'array-iterator', ...exprs];
    },
    [0xa2]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented game-over', ...exprs );
      }
      return [off, 'sys', 'game-over', ...exprs];
    },
    // what is done by pass command
    [0xa3]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented use-time', ...exprs );
      }
      return [off, 'sys', 'use-time', ...exprs];
    },
    [0xa4]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const slot = ctx.stack.pop();
        const participant = ctx.stack.pop();
        ctx.emit('talk-participant', { slot, participant });
      }
      return [off, 'sys', 'talk-participant', ...exprs];
    },
    // Probably frames.
    [0xa6]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented delay', ...exprs );
      }
      return [off, 'sys', 'delay', ...exprs];
    },
    [0xa7]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented delete', ...exprs );
      }
      return [off, 'sys', 'delete', ...exprs];
    },
    [0xa8]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const data1 = ctx.stack.pop();
        const data2 = ctx.stack.pop();
        const id = ctx.stack.pop();
        const quantity = ctx.stack.pop();
        const obj = ctx.src.data[15][id];
        ctx.stack.push( obj );
      }
      return [off, 'sys', 'create', ...exprs];
    },
    [0xa9]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented get-map-tile', ...exprs );
      }
      return [off, 'sys', 'get-map-tile', ...exprs];
    },
    //difference from B1?
    [0xab]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented take-item', ...exprs );
      }
      return [off, 'sys', 'take-item', ...exprs];
    },
    [0xac]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const max = ctx.stack.pop();
        const min = ctx.stack.pop();
        ctx.stack.push( Math.floor( Math.random()*(max-min)+min ) );
      }
      return [off, 'sys', 'random', ...exprs];
    },
    [0xad]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        // current usage gleaned from alaric's dialog that happens to also give the character
        // their class skills.
        // not yet sure of it's usage in magpie's subsequent dialog
        const unknown2 = ctx.stack.pop();
        const unknown1 = ctx.stack.pop();
        const id = ctx.stack.pop(); //
        const aspect = ctx.stack.pop(); //
        const character = ctx.stack.pop();
        const unknown0 = ctx.stack.pop();
        const typeid = ctx.stack.pop();
        const type = types[typeid];

        if (type) {
          const obj = new type( ctx.src, unknown0, aspect, id, unknown1, unknown2 );
          if (character) {
            character.skills.push( obj );
          }
          ctx.stack.push( obj );
        } else {
          console.warn( 'don\'t know how to create new', typeid, unknown0, character, aspect, id, unknown1, unknown2 );
        }

      }
      return [off, 'new', ...exprs];
    },
    // propaspect, data. returns char.
    [0xae]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented who-has-item', ...exprs );
      }
      return [off, 'sys', 'who-has-item', ...exprs];
    },
    [0xb1]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented remove-item', ...exprs );
      }
      return [off, 'sys', 'remove-item', ...exprs];
    },
    // (str prompt, int min, int max (inclusive))
    [0xb4]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented modal-number-input', ...exprs );
      }
      return [off, 'sys', 'modal-number-input', ...exprs];
    },
    //returns number of grains remaining, >=0
    [0xb7]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented weight-capacity', ...exprs );
      }
      return [off, 'sys', 'weight-capacity', ...exprs];
    },
    [0xB8]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented get-weight', ...exprs );
      }
      return [off, 'sys', 'get-weight', ...exprs];
    },
    [0xB9]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented join-party', ...exprs );
      }
      return [off, 'sys', 'join-party', ...exprs];
    },
    [0xba]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented leave-party', ...exprs );
      }
      return [off, 'sys', 'leave-party', ...exprs];
    },
    // doesn't quite work, might be dead code
    [0xbb]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented modal-party-selector', ...exprs );
      }
      return [off, 'sys', 'modal-party-selector', ...exprs];
    },
    [0xbc]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented is-in-party', ...exprs );
      }
      return [off, 'sys', 'is-in-party', ...exprs];
    },
    [0xbd]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented pass-time', ...exprs );
      }
      return [off, 'sys', 'pass-time', ...exprs];
    },
    [0xbe]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented update-lighting', ...exprs );
      }
      return [off, 'sys', 'update-lighting', ...exprs];
    },
    [0xbf]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const v3 = ctx.stack.pop();
        const zoneport = ctx.stack.pop();
        const v1 = ctx.stack.pop();
        ctx.emit( 'change-zone', { v1, zoneport, v3 } );
      }
      return [off, 'sys', 'change-zone', ...exprs];
    },
    [0xc0]: async (res,off,ctx) => {
      let exprs;
      const offset = off;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const buttons = ctx.stack.pop();
        const items = ctx.stack.pop();
        const format = ctx.stack.pop();
        const prompt = ctx.stack.pop();
        ctx.stack.push( await ctx.emit( 'show-menu', { prompt, format, items, buttons } ) );
      }
      return [off, 'sys', 'show-menu', ...exprs];
    },
    [0xc1]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const bit = ctx.stack.pop();
        const obj = ctx.stack.pop();
        obj.flags |= 1 << bit;
      }
      return [off, 'sys', 'set-bit', ...exprs];
    },
    [0xc2]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const bit = ctx.stack.pop();
        const obj = ctx.stack.pop();
        obj.flags &= ~(1 << bit);
      }
      return [off, 'sys', 'clear-bit', ...exprs];
    },
    [0xc3]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented status-effect', ...exprs );
      }
      return [off, 'sys', 'status-effect', ...exprs];
    },
    [0xc4]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const bit = ctx.stack.pop();
        const obj = ctx.stack.pop();
        ctx.stack.push( obj.flags & (1 << bit) );
      }
      return [off, 'sys', 'test-bit', ...exprs];
    },
    [0xc5]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented emit-signal', ...exprs );
      }
      return [off, 'sys', 'emit-signal', ...exprs];
    },
    [0xc7]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented prop-list-iterator', ...exprs );
      }
      return [off, 'sys', 'prop-list-iterator', ...exprs];
    },
    [0xc8]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented container-iterator', ...exprs );
      }
      return [off, 'sys', 'container-iterator', ...exprs];
    },
    [0xc9]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented recursive-container-iterator', ...exprs );
      }
      return [off, 'sys', 'recursive-container-iterator', ...exprs];
    },
    [0xca]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented party-iterator', ...exprs );
      }
      return [off, 'sys', 'party-iterator', ...exprs];
    },
    [0xcb]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented location-iterator', ...exprs );
      }
      return [off, 'sys', 'location-iterator', ...exprs];
    },
    [0xcc]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented equipment-iterator', ...exprs );
      }
      return [off, 'sys', 'equipment-iterator', ...exprs];
    },
    [0xce]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented enemy-iterator', ...exprs );
      }
      return [off, 'sys', 'enemy-iterator', ...exprs];
    },
    [0xcf]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented effect-iterator', ...exprs );
      }
      return [off, 'sys', 'effect-iterator', ...exprs];
    },
    // angers conspecific mons when 1 atkd
    [0xd0]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented monster-iterator', ...exprs );
      }
      return [off, 'sys', 'monster-iterator', ...exprs];
    },
    [0xd1]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented nearby-iterator', ...exprs );
      }
      return [off, 'sys', 'nearby-iterator', ...exprs];
    },
    [0xd2]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented play-note', ...exprs );
      }
      return [off, 'sys', 'play-note', ...exprs];
    },
    [0xd3]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const y = ctx.stack.pop();
        const x = ctx.stack.pop();
        const snd = ctx.stack.pop();
        ctx.src.sounds[ snd ].play();
      }
      return [off, 'sys', 'play-sound', ...exprs];
    },
    [0xd6]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const track = ctx.stack.pop();
        console.warn( '♫ play-music ♪', track );
        ctx.emit( 'play-music', track );
      }
      return [off, 'sys', 'play-music', ...exprs];
    },
    [0xd7]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented play-ambient-sound', ...exprs );
      }
      return [off, 'sys', 'play-ambient-sound', ...exprs];
    },
    [0xd8]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const level = ctx.stack.pop();
        ctx.emit( 'set-ambient-lighting', level );
      }
      return [off, 'sys', 'set-ambient-lighting', ...exprs];
    },
    [0xd9]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        // negative for indoors / no sky.
        const image = ctx.stack.pop();
        ctx.emit( 'set-landscape-image', image );
      }
      return [off, 'sys', 'set-landscape-image', ...exprs];
    },
    [0xda]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        ctx.emit( 'set-title', ctx.stack.pop() );
      }
      return [off, 'sys', 'set-title', ...exprs];
    },
    [0xdb]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented has-window', ...exprs );
      }
      return [off, 'sys', 'has-window', ...exprs];
    },
    [0xdc]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const value = ctx.state.get( ctx.stack.pop() );
        ctx.stack.push( value );
      }
      return [off, 'sys', 'get-state', ...exprs];
    },
    [0xdd]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const value = ctx.stack.pop();
        const key = ctx.stack.pop();
        ctx.state.set( key, value );
        ctx.emit( 'set-state', { key, value } );
      }
      return [off, 'sys', 'set-state', ...exprs];
    },
    // the address space does not seem to be
    // shared with DC/DD above.
    [0xde]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const value = ctx.state_flag.get( ctx.stack.pop() );
        ctx.stack.push( value );
      }
      return [off, 'sys', 'get-state-flag', ...exprs];
    },
    [0xdf]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const value = ctx.stack.pop();
        const key = ctx.stack.pop();
        ctx.state_flag.set( key, value );
        ctx.emit( 'set-state-flag', { key, value } );
      }
      return [off, 'sys', 'set-state-flag', ...exprs];
    },
    [0xe1]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented magic-aura-effect', ...exprs );
      }
      return [off, 'sys', 'magic-aura-effect', ...exprs];
    },
    [0xe2]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented shoot-effect', ...exprs );
      }
      return [off, 'sys', 'shoot-effect', ...exprs];
    },
    // momentarily flash a tile
    [0xe3]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented flash-tile', ...exprs );
      }
      return [off, 'sys', 'flash-tile', ...exprs];
    },
    // flash tile aligning it twixt two props
    [0xe4]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented hit-with-tile', ...exprs );
      }
      return [off, 'sys', 'hit-with-tile', ...exprs];
    },
    // Gets the next prop on the proplist in a
    // map or container. If you are calling it
    // on a prop on the map, it will skip over
    // props in containers. None returned if
    // called on the last prop in a container.
    // this is used to implement bellows.
    [0xe5]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented get-next-prop', ...exprs );
      }
      return [off, 'sys', 'get-next-prop', ...exprs];
    },
    [0xe6]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented refresh-view', ...exprs );
      }
      return [off, 'sys', 'refresh-view', ...exprs];
    },
    [0xe7]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const which = ctx.stack.pop();
        switch (which) {
        case 0:
          console.log('*earthquake*');
          break;
        case 2:
          console.log('*far sight*');
          break;

        case 3:

        case 4:

        default:
          console.warn( 'unknown special-view', which );
        }

      }
      return [off, 'sys', 'special-view', ...exprs];
    },
    [0xe8]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        ctx.emit('open-conversation');
      }
      return [off, 'sys', 'open-conversation', ...exprs];
    },
    [0xe9]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        ctx.emit('finish-conversation');
      }
      return [off, 'sys', 'finish-conversation', ...exprs];
    },
    [0xea]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        await ctx.emit('begin-cutscene');
      }
      return [off, 'sys', 'begin-cutscene', ...exprs];
    },
    [0xeb]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented end-cutscene', ...exprs );
      }
      return [off, 'sys', 'end-cutscene', ...exprs];
    },
    [0xec]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        ctx.emit('begin-slideshow');
      }
      return [off, 'sys', 'begin-slideshow', ...exprs];
    },
    [0xed]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        ctx.emit('end-slideshow');
      }
      return [off, 'sys', 'end-slideshow', ...exprs];
    },
    [0xee]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const text = ctx.stack.pop();
        const image = ctx.stack.pop();
        ctx.stack.push( await ctx.emit( 'slideshow', { text, image } ) );
      }
      return [off, 'sys', 'slideshow', ...exprs];
    },
    [0xf0]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented add-task', ...exprs );
      }
      return [off, 'sys', 'add-task', ...exprs];
    },
    [0xf1]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented finish-task', ...exprs );
      }
      return [off, 'sys', 'finish-tasks', ...exprs];
    },
    [0xf2]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const quest = ctx.stack.pop();
        const idx = ctx.stack.pop();
        ctx.emit( 'add-quest', quest );
      }
      return [off, 'sys', 'add-quest', ...exprs];
    },
    [0xf3]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented complete-quest', ...exprs );
      }
      return [off, 'sys', 'complete-quest', ...exprs];
    },
    [0xf4]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        ctx.emit('add-conversation-keyword', ctx.stack.pop());
      }
      return [off, 'sys', 'add-conversation-keyword', ...exprs];
    },
    [0xf5]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        const skill = ctx.stack.pop();
        const obj = ctx.stack.pop();
        ctx.stack.push( obj.skills.find( s => s.id & skill ) );
      }
      return [off, 'sys', 'get-skill', ...exprs];
    },
    [0xf6]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented set-view-position', ...exprs );
      }
      return [off, 'sys', 'set-view-position', ...exprs];
    },
    [0xf7]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented has-sight-line', ...exprs );
      }
      return [off, 'sys', 'has-sight-line', ...exprs];
    },
    [0xf8]: async (res,off,ctx) => {
      // takes a byte for the cmd. can ret vals.
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented cd-player-control', ...exprs );
      }
      return [off, 'sys', 'cd-player-control', ...exprs];
    },
    [0xfa]: async (res,off,ctx) => {
      // get prop from the propref field in map
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented get-prop', ...exprs );
      }
      return [off, 'sys', 'get-prop', ...exprs];
    },
    [0xfc]: async (res,off,ctx) => {
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented set-automapping', ...exprs );
      }
      return [off, 'sys', 'set-automapping', ...exprs];
    },
    [0xfd]: async (res,off,ctx) => {
      // parameter is cythera clut color
      let exprs;
      [off,...exprs] = await expressions( res, off+1, ctx );
      if (ctx) {
        console.warn( 'unimplemented set-background-color', ...exprs );
      }
      return [off, 'sys', 'set-background-color', ...exprs];
    },


  },
  {
    get: (target,key) => {
      const op = parseInt( key, 10 );
      if (op < 0x80) {
        return async (res,off,ctx) => {
          const str = readToOp( res, off );
          if (ctx) {
            await ctx.emit('string', str);
          }
          return [off+str.length,'string',str];
        };
      } else {
        return target[key] || noop;
      }
    }
  } );

/**
 * Table of stack expression opcodes
 *
 * Handles things like variables and literal values
 **/
const sops = new Proxy(
  {

    [0x41]: async (res,off,ctx) => {
      const byte = res.getInt8(off+1);
      if (ctx) {
        ctx.stack.push( byte );
      }
      return [off+2,'byte',byte];

    },

    [0x42]: async (res,off,ctx) => {
      const short = res.getInt16(off+1);
      if (ctx) {
        ctx.stack.push( short );
      }
      return [off+3,'short',short];
    },

    [0x43]: async (res,off,ctx) => {
      let word = res.getUint32(off+1);
      let type;
      [type,word] = word_val( word, ctx );
      if (ctx) {
        ctx.stack.push( word );
      }
      return [off+5,type,word];
    },

    [0x44]: async (res,off,ctx) => {
      let str = res.getCString(off+1);
      if (ctx) {
        ctx.stack.push(str);
      }
      return [off+str.length+2,'cstring',str];
    },

    [0x45]: async (res,off,ctx) => {
      const size = res.getUint16( off+1 );
      const data = makeScript( res.src, { resource: res, nested, size, offset: off+3 } );
      if (ctx) {
        ctx.stack.push( data );
      }
      return [off+size+3,'load-data',data];
    },

    [0x46]: async (res,off,ctx) => {
      if (ctx) {
        const idx = ctx.stack.pop();
        const array = ctx.stack.pop();
        ctx.stack.push( array.get( idx ) );
      }
      return [off+1,'index'];
    },

    [0x47]: async (res,off,ctx) => {
      const offset = res.getUint16(off+1);
      if (ctx) {
        const word = res.getUint32( offset );
        ctx.stack.push( word );
      }
      return [off+3,'load-word-at',offset];
    },

    [0x48]: async (res,off,ctx) => {
      const glob = res.getUint8(off+1);
      if (ctx) {
        ctx.stack.push( ctx.globals[ glob ] );
      }
      return [off+2,'global',globals[glob]||glob];
    },

    [0x49]: async (res,off,ctx) => {
      const resid = res.getUint16(off+1);
      const offset = res.getUint16(off+3);
      if (ctx) {
        let res = ctx.src.getResource( resid );
        let word = res.resource.getUint32( offset );
        if (word & 0x80000000) {
          const resid2 = (word >>> 16) & 0x7fff;
          if (resid2 !== resid) {
            res = ctx.src.getResource( resid2 );
          }
          word = makeScript( ctx.src, { nested, resource: res.resource, offset: (word & 0xffff) } );
        }
        ctx.stack.push( word );
      }
      return [off+5,'load-word-from-resource',resid,offset];
    },

    [0x4a]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        if (a instanceof ScriptArray) {
          ctx.stack.push( a.get( b ) );
        } else {
          ctx.stack.push( a + b );
        }
      }
      return [off+1,'add'];
    },
    [0x4b]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a - b );
      }
      return [off+1,'sub'];
    },
    [0x4c]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ctx.stack.pop() * ctx.stack.pop() );
      }
      return [off+1,'mul'];
    },
    [0x4d]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( (a / b)|0 );
      }
      return [off+1,'div'];
    },
    [0x4e]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a % b );
      }
      return [off+1,'mod'];
    },
    [0x4f]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a < b );
      }
      return [off+1,'lt'];
    },
    [0x50]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a <= b );
      }
      return [off+1,'le'];
    },
    [0x51]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a > b );
      }
      return [off+1,'gt'];
    },
    [0x52]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a >= b );
      }
      return [off+1,'ge'];
    },
    [0x53]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ctx.stack.pop() != ctx.stack.pop() );
      }
      return [off+1,'ne'];
    },
    [0x54]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ctx.stack.pop() == ctx.stack.pop() );
      }
      return [off+1,'eq'];
    },
    [0x55]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( -ctx.stack.pop() );
      }
      return [off+1,'neg'];
    },
    [0x56]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ctx.stack.pop() & ctx.stack.pop() );
      }
      return [off+1,'band'];
    },
    [0x57]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ctx.stack.pop() | ctx.stack.pop() );
        return [off+1];
      }
      return [off+1,'bor'];
    },
    [0x58]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ctx.stack.pop() ^ ctx.stack.pop() );
      }
      return [off+1,'bxor'];
    },
    [0x59]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ~ctx.stack.pop() );
      }
      return [off+1,'bnot'];
    },
    [0x5a]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a << b );
      }
      return [off+1,'shl'];
    },
    [0x5b]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a >> b );
      }
      return [off+1,'shr'];
    },
    [0x5c]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a && b );
        return [off+1];
      } else {
        return [off+1,'and'];
      }
    },
    [0x5d]: async (res,off,ctx) => {
      if (ctx) {
        const b = ctx.stack.pop();
        const a = ctx.stack.pop();
        ctx.stack.push( a || b );
      }
      return [off+1,'or'];
    },
    [0x5e]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( !ctx.stack.pop() );
      }
      return [off+1,'not'];
    },
    [0x5f]: async (res,off,ctx) => {
      if (ctx) {
        ctx.stack.push( ctx.stack.pop().length );
      }
      return [off+1,'len'];
    },
    [0x60]: async (res,off,ctx) => {
      const fidx = res.getUint8(off+1);
      const field = fields[fidx] || fidx;
      if (ctx) {
        const obj = ctx.stack.pop();
        ctx.stack.push( field in obj );
      }
      return [off+2,'has-field',field];
    },

    [0x61]: async (res,off,ctx) => {
      const fidx = res.getUint8(off+1);
      const field = fields[fidx] || fidx;
      const idx = res.getUint8(off+2);
      if (ctx) {
        const obj = ctx.stack.pop();
        ctx.stack.push( obj.constructor[ field ][ idx ] );
      }
      return [off+3,'class-field',field,idx];
    },

    [0x62]: async (res,off,ctx) => {
      const fidx = res.getUint8(off+1);
      const field = fields[fidx] || fidx;
      if (ctx) {
        const obj = ctx.stack.pop();
        ctx.stack.push( obj[ field ] );
      }
      return [off+2,'get-field',field];
    },

    [0x63]: async (res,off,ctx) => {
      const typeid = res.getUint8(off+1);
      const type = casts[ typeid ];
      if (!type) {
        throw new Error( `attempt to cast to unknown type 0x${hex(typeid)}` );
      }
      if (ctx) {
        const obj = ctx.stack.pop();
        if (obj instanceof type) {
          ctx.stack.push( obj );
        } else {
          ctx.stack.push( new type( ctx.src, obj ) );
        }
      }
      return [off+2,'cast',type];
    },

    [0x64]: async (res,off,ctx) => {
      const type = res.getUint8(off+1);
      const cast = casts[ type ] || type;
      if (ctx) {
        const obj = ctx.stack.pop();
        ctx.stack.push( obj instanceof cast );
      }
      return [off+2,'is-type',cast];
    },

  },
  {
    get: (target,key) => {
      const op = parseInt( key, 10 );
      if (op <= 0x3f) {
        return async (res,off,ctx) => {
          const n = res.getUint8(off);
          if (ctx) {
            ctx.stack.push( vari( ctx, n ) );
          }
          return [off+1,'var',var_name(n)];
        };
      } else {
        return target[key] || ops[key];
      }
    }
  });

export class ScriptFunction {
  constructor (src, res, opts={}) {
    this.src = src;
    this.resource = res;
    this.offset = opts.offset || 0;
    this.end = opts.size ? this.offset + opts.size : res.byteLength;

    const typecode = res.getUint8( this.offset );
    console.assert( typecode === 0x81, 'expecting typecode to be 0x81, instead got 0x' + hex( typecode ) );

    this.param_count = res.getUint8( this.offset+1 );
    console.assert( this.param_count < 0x10, 'function takes too many parameters: ' + this.param_count );

    this.local_count = res.getUint8( this.offset+2 );
    console.assert( this.local_count < 0x30, 'function has too many locals: ' + this.local_count );

  }

  get body () {
    return new Promise( async (resolve) => {
      if (!this._body) {
        const res = this.resource;
        const body = [];
        let offset = 3+this.offset;

        while (offset < this.end) {

          const op = res.getUint8(offset);
          try {
            // name is the operation name
            // data is any additional data the operation needs
            const [next,name,...data] = await ops[op]( res, offset, null, this.end );
            // save the offset for gotos, hopefully there aren't any that go into nested expressions
            body.push( [offset,name,...data] );
            offset = next;
          } catch (e) {
            console.error( 'res', hex( res.resid ), 'offset', offset, 'op', hex(op), e.stack );
            throw e;
          }

        }
        this._body = body;
      }
      resolve( this._body );
    } );
  }

  set dirty (val) {
    if (val) {
      this._body = null;
    }
  }

  async toString () {
    if (!this._toString) {

      this._toString = `(ScriptFunction [${range(this.param_count).map( n => var_name( n | 0x30 ) ).join(' ')}]`
        + (this.local_count > 0 ? `\n  (local ${range(this.local_count).map( var_name ).join(' ')})` : '')
        + '\n  ' + (await this.body).map( tos ).join('\n  ') + ')';
    }
    return this._toString;
  }

  async call (...args) {

    console.assert(
      args.length === this.param_count,
      `function should be called with ${this.param_count} parameters, but was called with ${args.length}` );

    const context = {
      id: randomId(),
      fn: this,
      iter: new Map(),
      locals: Array( this.local_count ),
      stack: [],
      emit: this.src.globals.emit,
      globals: this.src.globals,
      state: this.src.globals.state,
      state_flag: this.src.globals.state_flag,
      src: this.src,
      args,
    };

    const res = this.resource;
    let offset = 3+this.offset;
    while (offset < res.byteLength) {

      const op = res.getUint8( offset );
      console.log( '%c%s %c%s', 'color:#aaa;font-weight:bold', hex(op), 'color:#aaa', Array.from( res.getBytes(offset+1,15) ).map( b => hex(b) ).join(' ') );
      console.log( '%c%d %s', 'color:#aaa', offset, tos( (await ops[op]( res, offset )).slice(1) ) );
      const [ off, name, result ] = await ops[op]( res, offset, context );
      offset = off;
      if (offset === -1) {
        return result;
      }
    }

  }

}

export class ScriptArray {
  constructor (src, res, opts={}) {

    this.src = src;
    this.resource = res;
    this.offset = opts.offset || 0;

    const u16 = res.getUint16( this.offset );
    const typecode = u16 & 0xf000;
    this.count = u16 & 0x0fff;
    console.assert( typecode === 0x9000, 'expecting typecode to be 0x9000, instead got 0x' + hex( typecode, 4 ) );

  }

  get array () {
    if (!this._array) {
      const res = this.resource;
      this._array = new Array( this.count );
      let offset = 2 + this.offset;
      for (let i = 0; i < this.count; ++i) {
        const op = res.getUint32( offset );
        // this is true sometimes but maybe just for top level arrays?
        // maybe the first two bytes used to be a pointer to the resid to load the data from?
        // console.assert( res.resid === 0x201 && (op >>> 16) === 0x9165 || ((op >>> 16) & 0x7fff) === res.resid );
        if (op & 0x80000000) {
          this._array[i] = makeScript( this.src, { nested, resource: res, offset: (op & 0xffff) } );
        } else {
          const [type,word] = word_val( op );
          this._array[i] = word;
        }
        offset += 4;
      }
    }
    return this._array;
  }

  set dirty (val) {
    if (val) {
      this._array = null;
    }
  }

  get end () { return 2 + this.offset + (this.count * 4) - 1; }

  get (idx) { return this.array[idx]; }
  set (idx,val) { return this.array[idx] = val; }
  slice (...args) { return this.array.slice( ...args ); }
  get length () { return this.array.length; }
  forEach (fn, self) { return this.array.forEach( fn, self ); }
  map (fn, self) { return this.array.map( fn, self ); }
  [Symbol.iterator]() {
    return this.array[Symbol.iterator]();
  }

  toString () {
    return `(ScriptArray ${this.array.map( repr ).join(' ')})`;
  }
}

/**
 * A table is a key value record with the byte layout as follows:
 * First 2 bytes = offset at which the keys are stored (at the end of the table)
 * Next `offset-2` bytes are the table data
 * Starting at `offset` are pairs of value/key entries:
 *   the value is 4 bytes and is either a raw value (eg 0x5000ffff === null) or
 *     a reference into the table data (eg. 0xRSID0002 where RSID is the table
 *     resource id & 0x8000 and 0x0002 is the offset of the table value)
 *  the key is just a two byte number as far as I know
 **/
export class ScriptTable {
  constructor (src, res, opts={}) {
    this.src = src;
    this.resource = res;
    this.offset = opts.offset || 0;

    const u16 = res.getUint16( this.offset );
    const typecode = u16 & 0xf000;
    this.count = u16 & 0x0fff;
    console.assert( typecode === 0xa000, 'expecting typecode to be 0xa000, instead got 0x' + hex( typecode, 4 ) );
  }

  get map () {
    if (!this._map) {
      const res = this.resource;
      this._map = new Map();
      let offset = 2+this.offset;
      const offsets = [];
      for (let i = 0; i < this.count; ++i) {
        const value = res.getUint32( offset );
        const key = res.getUint16( offset + 4 );
        if (!this._map.has(key) || this._map.get(key) === null) {
          if ((value & 0x80000000) && ((value >>> 16) & 0x7fff) === res.resid) {
            offsets.push( [value & 0xffff, key] );
            this._map.set( key, value );
          } else {
            const [type,val] = word_val( value );
            this._map.set( key, val );
          }
        }
        offset += 6;
      }

      offsets.sort( ([a],[b]) => a - b ).forEach( ([start,key],i) => {
        const end = i + 1 === offsets.length ? this.offset : offsets[i+1][0];
        const obj = makeScript( this.src, { nested, resource: res, offset: start, size: end - start } );
        this._map.set( key, obj );
        const next = obj.end + 1;
        if (next < end && !offsets.find( ([off]) => off === next )) {
          // sometimes there is data hidden between table entries, attempt to locate that and add it to the table
          this._map.set( `sub${hex(next,4)}`, makeScript( this.src, { nested, resource: res, offset: next, size: end - next } ) );
        }
      } );
    }
    return this._map;
  }

  set dirty (val) {
    if (val) {
      this._map = null;
    }
  }

  get end () { return 2 + this.offset + (this.count * 6) - 1; }

  forEach (fn, self) { return this.map.forEach( fn, self ); }
  has (key) { return this.map.has( key ); }
  get (key) { return this.map.get( key ); }
  set (key, value) { return this.map.set( key, value ); }
  delete (key) { return this.map.delete( key ); }
  entries () { return this.map.entries(); }
  keys () { return this.map.keys(); }
  values () { return this.map.values(); }

  toString () {
    return `(ScriptTable ${Array.from( this.map.entries() ).map( entry => entry.map( repr ).join(' ') ).join(', ')})`;
  }

}

export class ScriptClass {
  constructor (src, res, opts={}) {
    this.src = src;
    this.resource = res;
    this.offset = opts.offset || 0;

    const u16 = res.getUint16( this.offset );
    const typecode = u16 & 0x8000;
    const table_offset = u16 & 0xffff;
    console.assert( typecode === 0x0000, 'expecting typecode to be 0x0000, instead got 0x' + hex( typecode, 4 ) );

    this.table = new ScriptTable( src, res, { nested, offset: table_offset } );
  }

  forEach (fn, self) { return this.table.forEach( fn, self ); }
  has (key) { return this.table.has( key ); }
  get (key) { return this.table.get( key ); }
  set (key, value) { return this.table.set( key, value ); }
  delete (key) { return this.table.delete( key ); }
  entries () { return this.table.entries(); }
  keys () { return this.table.keys(); }
  values () { return this.table.values(); }

  toString () {
    return `(ScriptClass ${this.table})`;
  }
}

import Tile from './Tile.js';

const res = {};
export default class CompoundTile extends Tile {

  constructor (src, id, attrs, opts={}) {
    super( src, id, attrs, opts );
    this.composition = src.tile_compositions[ id - 0x1000 ];
  }

  draw (ctx, x=0, y=0) {
    const { src } = this;
    const comp = this.composition;
    const len = comp.length; // should always be 16
    for (let i = 0; i < len; ++i) {
      const { resid, tile, segment } = comp[i];
      if (!res[ resid ]) {
        res[ resid ] = src.getResource( resid );
      }
      res[ resid ].drawSegment( ctx, tile, segment, x + (i % 4) * 8, y + ((i/4)|0) * 8, { mask: this.requires_mask } );
    }
  }

}

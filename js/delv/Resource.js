import Binary from './Binary.js';

export default class Resource extends Binary {

  constructor (src, byteOffset, byteLength, { id=null, i=0, j=0, encrypted=false }={}) {

    super( src, byteOffset, byteLength );

    this.src = src;
    this.resid = id || resid( i, j );
    this.subindex = i;
    this.resource = this;
    if (encrypted) {
      this.decrypt();
    }

  }

  decrypt () {
    console.log( '%cdecrypting 0x' + hex( this.resid, 4 ), 'color:#aaa' );

    const len = this.byteLength;
    const prokey = this.resid;
    const m = ((prokey & 0x3f) << 2) + 1;
    const b = prokey >> 6;
    let key = prokey ^ (prokey >> 8);

    for (let i = 0; i < len; ++i) {
      key = (key * m + b) & 0xffff;
      this.setUint8( i, (this.getUint8(i) ^ key) & 0xff );
    }

  }

  entropy () {
    const counts = new Array(256).fill(0);
    const len = this.byteLength / 8 - 1;
    for (let i = 0; i < len; ++i) {
      ++counts[ this.getUint8( i ) ];
    }
    const base = len / 256.0;
    return 1 - counts.reduce( (acc, c) => acc + (c - base)**2, 0 )**0.5 / len;
  }

}

export function resid (i,j=0) {
  if (i instanceof Resource) {
    return i.resid;
  } else {
    if (i > 0xff || j > 0xff) {
      throw new Error('out of bounds');
    }
    return ((i+1)<<8) | j;
  }
}

export function hex (n,pad=2) {
  if (n instanceof Uint8Array) {
    return Array.from( n ).map( n => hex( n, pad ) ).join(' ');
  } else if (typeof n === 'number') {
    return n.toString(16).padStart(pad,'0');
  } else {
    return '?'.padStart(pad,'?');
  }
}

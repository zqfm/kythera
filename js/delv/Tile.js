import Resource from './Resource.js';
import Prop from './Prop.js';

export default class Tile {

  constructor (src, id, opts={}) {
    this.src = src;

    this.attributes = src.tile_attributes[ id ];
    this.tilesheet = src.tilesheets[ (id >>> 4) & 0xff ];

    if (/*!this.attributes ||*/ !this.tilesheet) {
      id = 0;
      this.attributes = src.tile_attributes[ 0 ];
      this.tilesheet = src.tilesheets[ 0 ];
    }

    this.tile_id = id;
    this.name = src.tile_names[ id ];

    this.requires_mask = this.attributes & 0xff000000;

  }

  get draw_priority () {
    return Tile.draw_priority( this.attributes );
  }

  get faux_prop () {
    if (!this._faux_prop) {
      const faux_prop = this.src.faux_props[ this.tile_id ];
      if (faux_prop) {
        this._faux_prop = new Prop( this.src, faux_prop );
      }
    }
    return this._faux_prop;
  }

  draw (ctx, x=0, y=0) {
    this.tilesheet.drawTile( ctx, this.tile_id & 0xf, x, y, { mask: this.requires_mask } );
  }

}

Tile.draw_priority = (attributes) => {
  if (!attributes) {
    return 0x400000;
  } else {
    //                attr
    // torch    0x00800000 0b0000 0000 1000 0000 0000 0000 0000 0000
    // cuirass  0x00000000 0b0000 0000 0000 0000 0000 0000 0000 0000
    // helmet   0x00400000 0b0000 0000 0100 0000 0000 0000 0000 0000
    // table    0x00520200 0b0000 0000 0101 0010 0000 0010 0000 0000
    // tableleg 0x00500000 0b0000 0000 0101 0000 0000 0000 0000 0000
    // shadow   0x00500000 0b0000 0000 0101 0000 0000 0000 0000 0000 <- should be under tableleg

    // mirror   0x00400010 0b0000 0000 0100 0000 0000 0000 0001 0000
    // "floor"  0x10000020 0b0001 0000 0000 0000 0000 0000 0010 0000 <- correctly under mirror

    // potion   0x00400000 0b0000 0000 0100 0000 0000 0000 0000 0000
    // table    0x00520200 0b0000 0000 0101 0000 0000 0000 0000 0000 <- correctly under potion

    // book     0x00400000 0b0000 0000 0100 0000 0000 0000 0000 0000
    // table    0x00520200 0b0000 0000 0101 0000 0000 0000 0000 0000 <- correctly under book

    // grimoire 0x00400001 0b0000 0000 0100 0000 0000 0000 0000 0001
    // table    0x00520200 0b0000 0000 0101 0000 0000 0000 0000 0000 <- correctly under grimoire

    // inkwell  0x00800000 0b0000 0000 1000 0000 0000 0000 0000 0000
    // table    0x00520200 0b0000 0000 0101 0000 0000 0000 0000 0000 <- correctly under inkwell

    // fancy wall pattern
    // "floor"  0x10100080 0b0001 0000 0001 0000 0000 0000 1000 0000 <- correctly under the below
    // wall column
    // "floor"  0x10000020 0b0001 0000 0000 0000 0000 0000 0010 0000 <- correctly over the above

    // fancy wall pattern
    // "floor"  0x10100080 0b0001 0000 0001 0000 0000 0000 1000 0000 <- should be under mirror
    // mirror   0x00400010 0b0000 0000 0100 0000 0000 0000 0001 0000

    //                     0b1010 1111 1100 1101 1111 1101 0011 1100
    return attributes & 0xafcdfd3c;

    // original from delvmod
    // return attributes & 0xbfcdfd1c;
  }
};

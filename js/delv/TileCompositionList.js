import Resource from './Resource.js';

export default class TileCompositionList {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {
    this.resource = new Resource( src, byteOffset, byteLength, opts );
  }

  /**
   * A composition is an array of 16 { resid, tile, segment }s
   * where the segment is an 8x8 chunk of the tile numbered like
   *
   *    0 4 8 c
   *    1 5 9 d
   *    2 6 a e
   *    3 7 b f
   *
   **/
  getComposition (tile=0) {
    return Array.from( this.resource.getShorts( tile * 32, 16 ) )
      .map( t => ({ resid: 0x8e00 | ((t>>4) & 0xff),
                    tile: t & 0xf,
                    segment: (t & 0xf000) >> 12 }) );
  }

}

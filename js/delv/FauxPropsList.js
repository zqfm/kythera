import Resource from './Resource.js';
import Prop from './Prop.js';

export default class FauxPropsList {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {
    this.src = src;
    this.resource = new Resource( src, byteOffset, byteLength, opts );
  }

  /**
   *
   **/
  getFauxProp (tile=0) {
    const word = this.resource.getUint16( tile * 2 );
    if (word) {
      return Prop.fromFauxProp( this.src, word );
    } else {
      return undefined;
    }
  }

}

import CompoundTile from './CompoundTile.js';

/**
 * Generates a canvas will all generated tiles
 **/
export default class AllTileSheet {

  constructor (src) {

    const canvas = this.canvas = document.createElement('canvas');
    canvas.width = 4096;
    canvas.height = 4096;
    const ctx = canvas.getContext('2d');

    for (let i = 0; i < 160; ++i) {
      const x = (i & 0x7f) << 5; // (i % 128) * 32
      const y = (i >>> 7)  << 9; // floor(i / 128) * 512
      const tilesheet = src.tilesheets[i];
      if (tilesheet) {
        ctx.drawImage( tilesheet.canvas, x, y );
      }
    }

    for (let i = 0; i < 160; ++i) {
      const x = (i & 0x7f) << 5; // (i % 128) * 32
      const y = ((i >>> 7)  << 9) + 256; // floor(i / 128) * 512

      for (let j = 0; j < 16; ++j) {

        const tile = new CompoundTile( src, i * 16 + j );
        if (tile) {
          tile.draw( ctx, x, j * 32 + y );
        }

      }
    }

  }

}

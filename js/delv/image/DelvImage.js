import Resource, { hex } from '../Resource.js';

export default class DelvImage {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {

    this.resource = new Resource( src, byteOffset, byteLength, opts );
    this.src = src;

    let icursor = 0;
    if ('width' in opts && 'height' in opts) {
      this.width = this.logical_width = opts.width;
      this.height = this.logical_height = opts.height;
      this.wflags = this.hflags = 0;
    } else {
      // assume load from header
      const [a,b,c,d] = this.resource.getBytes(0,4);
      // header in binary

      // aaaa aaaa bbbb bbbb cccc cccc dddd dddd
      // wwww wwww wwww wwWW hhhh hhhh hhhh hhhH

      this.width = ((a << 6) + ((b & 252) >> 2)) << 2;  // w
      this.wflags = b & 3;                              // W
      this.height = ((c << 7) + ((d & 254) >> 1)) << 1; // h
      this.hflags = d & 1;                              // H

      if (this.wflags) {
        this.logical_width = this.width + 4;
        this.width += this.wflags;
      } else {
        this.logical_width = this.width;
      }

      if (this.hflags) {
        this.logical_height = this.height + this.hflags;
        this.height += this.hflags;
      } else {
        this.logical_height = this.height;
      }

      icursor = 4;

    }

    this.canvas = this.canvasify( this.decompress( icursor ), opts.compressed !== false );

  }

  /**
   * interprets a mini DSL for describing image data
   * op codes are 1 byte
   *
   * short copy   0aaa aaaa bbbb bbbb
   * long copy    10aa aaaa bbbb bbbb cccc cccc
   * pixel data   1100 aaaa
   * short data   1101 aaaa
   * short run    1110 aaaa bbbb bbbb
   * long run     1111 0000 bbbb bbbb cccc cccc
   * terminate    1111 1111
   *
   * returns a new ArrayBuffer with the image data
   **/
  decompress (icursor=0) {
    let ocursor = 0; // the index at which data will be written to imageData
    const imageData = new ArrayBuffer( this.logical_width * this.logical_height );
    const dv = new DataView( imageData );

    const res = this.resource;

    // copy pixel data into image
    const cdata = len => {
      const pixels = res.getBytes( icursor, len );
      for (let i = 0; i < len; ++i) {
        dv.setUint8( ocursor + i, pixels[ i ] );
      }
      ocursor += len;
    };

    // copy pixels from one part of image to another
    const copy = (length, origin) => {
      const abs_origin = ocursor + origin;
      const copy_width = -origin;
      for (let n = 0; n < length; ++n) {
        dv.setUint8( ocursor, dv.getUint8( abs_origin + (n % copy_width) ) );
        ++ocursor;
      }
    };

    // set length pixels to color
    const run = (length, color) => {
      const maxcursor = ocursor + length;
      while (ocursor < maxcursor) {
        dv.setUint8( ocursor, color );
        ++ocursor;
      }
    };

    while (icursor < res.byteLength) {
      // get the first byte, the opcode, represented in comments below as "aaaa aaaa"
      // bytes are labeled a, b, and c where b and c are futher arguments to the opcode
      const op = res.getUint8( icursor++ );

      if (op < 0x80) {
        // short copy operation 0x00 - 0x7f

        // aaaa aaaa bbbb bbbb
        // 0iii iiii iiiL Llll

        const b = res.getUint8( icursor++ );
        const index = -((((b & 0b11100000) << 2) | (op & 0b1111111)) + 1); // i
        const length = (b & 0b111) + 3;                                 // l
        const literals = (b & 0b11000) >> 3;                            // L

        cdata( literals );
        icursor += literals;
        copy( length, index );

      } else if (op < 0xc0) {
        // long copy operation 0x80 - 0xbf

        // aaaa aaaa bbbb bbbb cccc cccc
        // 10ii iiii iiil llll iiii iiLL

        const [b,c] = res.getBytes( icursor, 2 );
        icursor += 2;
        const index = -((((c & 0b11111100) << 7) | ((b & 0b11100000) << 1) | (op & 0b111111)) + 1); // i
        const length = (b & 0b11111) + 3;                                                        // l
        const literals = c & 0b11;                                                               // L

        cdata( literals );
        icursor += literals;
        copy( length, index );

      } else if (op < 0xd0) {
        // pixel data 0xc0 - 0xcf

        // aaaa aaaa
        // 1100 ssss

        const size = ((op & 0b1111) + 1) * 4; // s

        cdata( size );
        icursor += size;

      } else if (op < 0xe0) {
        // short data 0xd0 - 0xdf

        // aaaa aaaa
        // 1101 LLLL

        const literals = op & 0b1111; // L
        cdata( literals );
        icursor += literals;

      } else if (op < 0xf0) {
        // short run 0xe0 - 0xef

        // aaaa aaaa bbbb bbbb
        // 1110 llll cccc cccc

        const b = res.getUint8( icursor++ );
        const length = (op & 0b1111) + 3; // l
        const color = b;                  // c

        run( length, color );

      } else if (op === 0xf0) {
        // long run 0xf0

        // aaaa aaaa bbbb bbbb cccc cccc
        // 1111 0000 llll llll cccc cccc

        const [b,c] = res.getBytes( icursor, 2 );
        icursor += 2;
        const length = b + 3; // l
        const color = c;      // c

        run( length, color );

      } else if (op === 0xff) {
        // terminate 0xff
      } else {
        throw new Error(`Unknown opcode 0x${hex( op )} at 0x${hex( icursor )}`);
      }
    }

    return imageData;
  }

  canvasify (data, compressed) {
    const w = this.logical_width;
    const h = this.logical_height;
    const canvas = document.createElement('canvas');
    canvas.width = w;
    canvas.height = h;
    const ctx = canvas.getContext('2d');

    const dv = compressed
          ? new DataView( data )
          : new DataView( data, this.resource.byteOffset, this.resource.byteLength );

    for (let y = 0; y < h; ++y) {
      for (let x = 0; x < w; ++x) {

        const v = dv.getUint8( y * w + x );
        if (v !== 0) {
          const color = CLUT( dv.getUint8( y * w + x ) );

          ctx.fillStyle = '#' + color;
          ctx.fillRect( x, y, 1, 1 );
        }

      }
    }

    return canvas;
  }

  draw (ctx, x=0, y=0) {
    ctx.drawImage( this.canvas, x, y );
  }

}

const pal = [
    'ffffff', '0000a8', '00a800', '00a8a8', 'a80000', 'a800a8', 'a85400', 'a8a8a8',
    '545454', '5454fc', '54fc54', '54fcfc', 'fc5454', 'fc54fc', 'fcfc54', 'fcfcfc',
    'fcfcfc', 'ececec', 'd8d8d8', 'c8c8c8', 'b8b8b8', 'a8a8a8', '989898', '848484',
    '747474', '646464', '545454', '444444', '343434', '202020', '101010', '080808',
    'fcf400', 'f8c800', 'f4a400', 'ec8000', 'e86000', 'e44000', 'e02000', 'dc0000',
    'c80000', 'b40000', 'a00000', '8c0000', '7c0000', '680000', '540000', '400000',
    'fcfcfc', 'fcf4c0', 'fcec84', 'fce448', 'fcdc38', 'fcd024', 'fcc814', 'fcb800',
    'e89000', 'd07000', 'bc5400', 'a83c00', '942800', '7c1800', '680800', '540000',
    'e8905c', 'dc7848', 'd0603c', 'c04c2c', 'b4381c', 'a82414', '9c1008', '900000',
    '800000', '6c0000', '5c0000', '480000', '380000', '240000', '100000', '000000',
    'f8fcd8', 'f4fcb8', 'e8fc9c', 'e0fc7c', 'd0fc5c', 'c4fc40', 'b4fc20', 'a0fc00',
    '90e400', '80cc00', '74b400', '609c00', '508400', '447000', '345800', '284000',
    'd8fcd8', 'bcfcb8', '9cfc9c', '80fc7c', '60fc5c', '40fc40', '20fc20', '00fc00',
    '00e400', '04cc00', '04b400', '049c00', '088400', '047000', '045800', '044000',
    'd8ecfc', 'b8dcfc', '9cd0fc', '7cbcfc', '5cacfc', '4094fc', '2084fc', '0070fc',
    '0068e4', '005ccc', '0058b4', '00509c', '004484', '003c70', '003058', '002440',
    'fcc87c', 'f0b870', 'e8a868', 'dc9c60', 'd09058', 'c88450', 'bc784c', 'b46c44',
    'a0643c', '906034', '80542c', '6c4c24', '5c401c', '483818', '382c10', '28200c',
    'fcd8fc', 'fcb8fc', 'fc9cfc', 'fc7cfc', 'fc5cfc', 'fc40fc', 'fc20fc', 'fc00fc',
    'e000e4', 'c800cc', 'b400b4', '9c009c', '840084', '6c0070', '580058', '400040',
    'fce8dc', 'fce0d0', 'fcd8c4', 'fcd4bc', 'fcccb0', 'fcc4a4', 'fcbc9c', 'fcb890',
    'e8a47c', 'd0946c', 'bc8458', 'a8744c', '94643c', '805830', '684824', '543c1c',
    'fce8dc', 'f4c8b4', 'e8b090', 'e09470', 'd47850', 'cc6034', 'c44818', 'bc3400',
    'a82800', '981c00', '881400', '781000', '680800', '580400', '480000', '380000',
    'fcf46c', 'f0f060', 'dce454', 'ccdc48', 'b8d040', 'a8c434', '94b82c', '84b024',
    '749820', '64841c', '506c14', '405810', '30400c', '202c08', '101404', '000000',
    'fcfcfc', 'e8e8f0', 'd4d4e8', 'c0c4dc', 'b4b4d0', 'a0a0c8', '9494bc', '8484b4',
    '74749c', '646484', '505470', '404058', '303044', '20202c', '101018', '000000',
    'fc0000', 'fc1c00', 'fc4000', 'fc6000', 'fc7c00', 'fc9800', 'fcbc00', 'fcdc00',
    '0010fc', '1028fc', '1c44fc', '2c5cfc', '3874fc', '4484fc', '5498fc', '60a8fc',
    'd02094', 'dc34c0', 'ec48e8', 'ec60fc', '704820', '84542c', '9c6038', 'b56d45',
    '24a800', '1cbc00', '10d000', '00e400', '000000', '000000', 'fcf4c0', '000000',
];

export function CLUT (n,frame=0) {
  if (n < 0xe0) {
    return pal[n];
  } else if (n < 0xf0) {
    return pal[ (n & 0xf8) | ((n + frame) & 7) ];
  } else if (n < 0xfc) {
    return pal[ (n & 0xfc) | ((n + frame) & 3) ];
  } else {
    return pal[n];
  }
}

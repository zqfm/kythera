import DelvImage, { CLUT } from './DelvImage.js';

const TILE_WIDTH = 32;
const TILE_HEIGHT = 32;
const TILE_BYTE_LENGTH = TILE_WIDTH * TILE_HEIGHT;

const SEGMENT_WIDTH = 8;
const SEGMENT_HEIGHT = 8;

export default class TileSheet extends DelvImage {
  constructor (src, byteOffset, byteLength, opts={}) {
    super( src, byteOffset, byteLength, { width: 32, height: 512, ...opts } );
  }

  drawTile (ctx, tile=0, x=0, y=0, { rotate=false, mask=false, offset=[0,0] }={}) {

    // console.log( 'drawTile %c+', `color:transparent;font-size:1px;padding:16px;line-height:0px;display:block;background:url(${this.canvas.toDataURL()}) no-repeat;background-position:0px ${-tile*TILE_HEIGHT}px`, tile, 'x', x, 'y', y, 'rotate', rotate, 'offset', offset );

    ctx.save();

    let [xo,yo] = offset;

    if (rotate) {
      [yo,xo] = [xo,yo];
    }

    ctx.translate( x - xo, y - yo );

    if (rotate) {
      ctx.rotate( -Math.PI / 2 );
      ctx.scale( -1, 1 );
    }

    if (!mask) {
      ctx.fillStyle = '#' + CLUT(0);
      ctx.fillRect( 0, 0, TILE_WIDTH, TILE_HEIGHT );
    }

    ctx.drawImage(
      this.canvas,
      0,
      tile * TILE_HEIGHT,
      TILE_WIDTH,
      TILE_HEIGHT,
      0,
      0,
      TILE_WIDTH,
      TILE_HEIGHT );

    ctx.restore();

  }

  drawSegment (ctx, tile=0, segment=0, x=0, y=0, { mask=false }) {

    /**
     * https://github.com/BryceSchroeder/delvmod/wiki/Composed-Tiles
     *
     * Segments are numbered starting from the 8x8 pixel chunk in the upper left
     * corner of the source tile, but going top to bottom, left to right.
     **/
    const sox = Math.floor( segment / 4 ) * SEGMENT_WIDTH;
    const soy = (segment % 4) * SEGMENT_HEIGHT;

    if (!mask) {
      ctx.fillStyle = '#' + CLUT(0);
      ctx.fillRect( x, y, SEGMENT_WIDTH, SEGMENT_HEIGHT );
    }

    // console.log( 'drawSegment %c+', `color:transparent;font-size:1px;padding:4px;line-height:0px;display:block;background:url(${this.canvas.toDataURL()}) no-repeat;background-position:${-sox}px ${-(tile*TILE_HEIGHT+soy)}px`, tile, segment, x, y );

    ctx.drawImage(
      this.canvas,
      sox,
      tile * TILE_HEIGHT + soy,
      SEGMENT_WIDTH,
      SEGMENT_HEIGHT,
      x,
      y,
      SEGMENT_WIDTH,
      SEGMENT_HEIGHT );

  }
}

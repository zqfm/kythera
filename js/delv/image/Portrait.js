import DelvImage from './DelvImage.js';

export default class Portrait extends DelvImage {
  constructor (src, byteOffset, byteLength, opts={}) {
    super( src, byteOffset, byteLength, { width: 64, height: 64, ...opts } );
  }
}

import DelvImage from './DelvImage.js';

export default class SkillIcon extends DelvImage {
  constructor (src, byteOffset, byteLength, opts={}) {
    super( src, byteOffset, byteLength, { compressed: false, width: 32, height: 16, ...opts } );
  }

  /**
   * Skill icons are not compressed
   **/
  decompress () {
    return this.src.buffer;
  }
}

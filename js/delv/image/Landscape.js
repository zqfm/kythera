import DelvImage from './DelvImage.js';

const width = 288;
const height = 32;

export default class Landscape extends DelvImage {
  constructor (src, byteOffset, byteLength, opts={}) {
    super( src, byteOffset, byteLength, { width, height, ...opts } );
  }

  drawBackground (ctx, _global0x04, x=0, y=0) {
    const timeOfDay = _global0x04;
    //ctx.fillStyle = '#000000';
    ctx.fillStyle = '#36a8a9';
    ctx.fillRect( x, y, x + width, y + height );
  }
}

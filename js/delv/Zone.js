import Resource from './Resource.js';
import Binary from './Binary.js';
import Tile from './Tile.js';
import Prop from './Prop.js';
import CompoundTile from './CompoundTile.js';

/**
 *
 **/
export default class Zone {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {

    this.src = src;
    this.resource = new Resource( src, byteOffset, byteLength, opts );
    const res = this.resource;

    this.width = res.getUint16( 0x00 );
    this.height = res.getUint16( 0x02 );

    this.roof_layer_size = res.getUint16( 0x06 );
    this.roof_underlayer_size = res.getUint16( 0x08 );

    // 0 to show blackness, anything else to repeat the last tile on the edge
    this.horizontal_edge_propagation = res.getUint8( 0x0a );
    this.vertical_edge_propagation = res.getUint8( 0x0b );

    this.exit_zoneport_north = res.getUint16( 0x0c );
    this.exit_zoneport_east = res.getUint16( 0x0e );
    this.exit_zoneport_south = res.getUint16( 0x10 );
    this.exit_zoneport_west = res.getUint16( 0x12 );

    const roof_length = 0x40 * this.roof_layer_size + 0x40 * this.roof_underlayer_size;

    this.roof_data = new Binary( res.buffer, res.byteOffset + 0x20, roof_length );
    this.map_data = new Binary( res.buffer, res.byteOffset + 0x20 + roof_length, this.width * this.height * 2 );

    this.zeroTile = new Tile( src, 0 );

    const prop_list = src.prop_lists[ this.resource.resid - 0x8000 ];
    const props = this.props = {};
    if (prop_list) {
      for (let i = 0; i < prop_list.byteLength; i += 16) {
        const prop = Prop.load( src, prop_list.byteOffset + i, { zone: this, index: i / 16 } );
        if (prop) {
          const { x, y } = prop;
          if (!props[x]) { props[x] = {}; }
          if (!props[x][y]) { props[x][y] = []; }
          props[x][y].push( prop );
        }
      }
    }

    for (let x = 0; x < this.width; ++x) {
      for (let y = 0; y < this.height; ++y) {
        const tile = this.getTile( x, y );
        const fp = tile && tile.faux_prop;
        if (fp) {
          if (!props[x]) { props[x] = {}; }
          if (!props[x][y]) { props[x][y] = []; }
          props[x][y].push( fp );
        }
      }
    }

    Object.values( props ).forEach( l => {
      Object.values( l ).forEach( props => {
        props.sort( (a,b) => (Tile.draw_priority(a.attributes) - Tile.draw_priority(b.attributes)) || (b.tile_id - a.tile_id) );
      } );
    } );

  }

  get zone_id () {
    return this.resource.resid - 0x8000;
  }

  getTile (x, y) {

    const clamp = (n,d,e) => {
      if (n < 0) {
        if (e) {
          return (e + (n & (e-1))) & (e-1);
        } else {
          return this.zeroTile;
        }
      } else if (n >= d) {
        if (e) {
          return d - (e - (n & (e-1)));
        } else {
          return this.zeroTile;
        }
      }
      return n;
    };

    x = clamp( x, this.width, this.horizontal_edge_propagation );
    y = clamp( y, this.height, this.vertical_edge_propagation );

    if (x === this.zeroTile || y === this.zeroTile ) {
      return this.zeroTile;
    }

    const tileid = this.map_data.getUint16( (x + y * this.width) * 2 );
    if (tileid < 0x1000) {
      return new Tile( this.src, tileid );
    } else {
      return new CompoundTile( this.src, tileid );
    }
  }

  get rooms () {
    if (!this._rooms) {
      this._rooms = Object.values( this.props ).reduce(
        (acc, col) => Object.values( col ).reduce(
          (acc, props) => [ ...acc, ...props.filter( p => p.flags === 0x42 && p.aspect === 8 ) ],
          acc ),
        [] );
    }
    return this._rooms;
  }

  get npcs () {
    return this.src.schedules.reduce( (acc, schedules, i) => {
      if (schedules.find( ({zone}) => zone === this.zone_id )) {
        acc.push( this.src.characters[i] );
      }
      return acc;
    }, [] );
  }

  getRoom (x, y) {
    return this.rooms.find(
      room => x >= (room.x - Math.ceil( room.d1 / 2 ))
        && x <= (room.x + Math.floor( room.d1 / 2 ))
        && y >= (room.y - Math.ceil( room.d2 / 2 ))
        && y <= (room.y + Math.floor( room.d2 / 2 )) );
  }

  getProps (x, y) {
    const col = this.props[x] || {};
    const col2 = this.props[x+1] || {};
    return (col[y] || [])
      .concat( (col[y+1] || []).filter( prop => prop.extension_n ).map( prop => prop.extension_n ) )
      .concat( (col2[y] || []).filter( prop => prop.extension_w ).map( prop => prop.extension_w ) )
      .concat( (col2[y+1] || []).filter( prop => prop.extension_nw ).map( prop => prop.extension_nw ) );
  }

  addPropAt (prop, x, y) {
    prop._x = x;
    prop._y = y;

    if (!this.props[x]) { this.props[x] = {}; }
    if (!this.props[x][y]) { this.props[x][y] = []; }

    this.props[x][y].push( prop );
  }

  removeProp (prop) {
    const { x, y } = prop;
    if (this.props[x] && this.props[x][y]) {
      this.props[x][y] = this.props[x][y].filter( p => p !== prop );
    }
  }

  movePropTo (prop, x, y) {
    this.removeProp( prop );
    this.addPropAt( prop, x, y );
  }

  draw (ctx, ox=0, oy=0, { width=40, height=16, hideProps=false, hideFauxProps=false, hideDebugProps=true }) {
    const props = [];

    ox -= Math.ceil( width / 2 );
    oy -= Math.ceil( height / 2 );

    for (let y = -1; y < height + 1; ++y) {
      for (let x = -1; x < width + 1; ++x) {
        const tile = this.getTile( x + ox, y + oy );
        if (tile) {
          tile.draw( ctx, x * 32, y * 32 );
          if (!hideProps) {
            this.getProps( x + ox, y + oy )
              .filter( p => !((hideFauxProps && p.faux_prop) || (hideDebugProps && !p.visible)) )
              .forEach( prop => prop.draw( ctx, x * 32, y * 32 ) );
          }
        }
      }
    }
  }

}

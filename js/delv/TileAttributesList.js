import Resource from './Resource.js';

export default class TileAttributesList {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {
    this.resource = new Resource( src, byteOffset, byteLength, opts );
  }

  getAttribute (tile=0) {
    try {
      return this.resource.getUint32( tile * 4 );
    } catch (e) {
      console.warn( e );
      return 0;
    }
  }

}

import { GLOBAL } from './constants.js';
import Prop from './Prop.js';

export default class Character {

  constructor (src, from) {
    this.src = src;
    this.flags = 0;
    this.status_flags = 1;
    this.bit_flags = 0;
    this.skills = [];
    this.visible = true;
    this.inventory = [];
    switch (typeof from) {
    case 'object':
      Object.assign( this, from );
      break;
    case 'number':
      this.id = from;

      /**
       * mapping of character -> prop ids (until I find it in the data)
       *
       * technique for finding the prop_id:
       * manually look at the tile_id for the character in the debug window
       * [...Array(1024).keys()].map( i => scenario.prop_tiles[i] ).findIndex( tile_id => tile_id === <the tile id> )
       **/
      this.prop_id = ({
        2: 34, // Alaric
        3: 35, // Magpie
        4: 46, // Hadrian
        5: 49, // Emesa
        6: 116, // Hector
        7: 46, // guard
        8: 46, // guard
        9: 46, // guard
        109: 117, // Demodocus
      })[from];
      break;
    }
  }

  get x () { return this._x; }
  get y () { return this._y; }

  set x (x) { this._x = x; }
  set y (y) { this._y = y; }

  get name () {
    if (this.id === 1) {
      return this.src.globals[ GLOBAL.PLAYER_NAME ];
    } else if (this.introduced) {
      return this.src.getResource( 0x201 ).get( this.id );
    } else {
      return this.prop.name;
    }
  }

  get schedule () {
    return this.src.schedules[ this.id ];
  }

  get script_class () {
    return this.src.character_scripts[ this.id ];
  }

  get portrait () {
    return this._portrait || this.src.portraits[ this.id - 1 ];
  }

  get aspect () {
    return this.prop.aspect;
  }

  set aspect (aspect) {
    this.prop.aspect = aspect;
  }

  async init (...args) {
    if (this.script_class) {
      const init = this.script_class.get(0);
      if (init) {
        await init.call( this, ...args );
      }
    }
  }

  get prop () {
    if (!this._prop) {
      this._prop = new Prop( this.src, { prop_id: this.prop_id, aspect: 1 } );
    }
    return this._prop;
  }

  draw (ctx, x, y, { aspect }={}) {
    return this.prop.draw( ctx, x, y, { aspect } );
  }

}

Character.get = (ctx, word) => {
  return ctx.src.characters[ word ];
}

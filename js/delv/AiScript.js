import Resource, { hex } from './Resource.js';

export default class AiScript {
  constructor (src, offset, length, opts) {
    this.src = src;
    const res = this.resource = new Resource( src, offset, length, opts );
    this.name = res.getPascalString();
    this.body_offset = 0x1f;
  }

  /**
   * After the script name, there is some data that doesn't seem to exist
   * in the text .ai script files
   **/
  get preamble () {
    const res = this.resource;
    const start = res.getUint8() + 1;

    return res.getBytes( start, 0x1f - start );
  }

  toString () {

    const res = this.resource;
    let offset = this.body_offset;

    const end = res.byteLength - 1;
    const lines = [];
    while (offset < end) {

      const op = res.getUint16( offset );

      const sid = res.getUint8( offset + 3 );
      const subject = sid
            ? subjects[ sid ] || (console.warn('unknown subject', '0x' + hex(sid) ),'unknown')
            : '';

      const arg = res.getUint8( offset + 4 );

      const selector = Array.from( res.getBytes( offset + 5, 3 ) )
            .filter( sel => sel )
            .map( sel => selectors[ sel ] || (console.warn('unknown selector', '0x' + hex(sel) ),'UnknownSelector') )
            .join('.');

      if (op > 0x64) {
        // conditionals

        const pid = res.getUint8( offset + 2 );
        const predicate = predicates[ pid ]
              || (console.warn('unknown predicate', '0x' + hex(pid) ),'UnknownPredicate');

        lines.push([

          op === 0x65 ? 'IF'
            : op === 0x66 ? 'IF NOT'
            : (console.warn('unknown operation', '0x' + hex(op) ), hex(op)),

          predicate + '('
            + subject
            + (selector ? '.' + selector : '')
            + (arg ? (subject ? ',' : '') + arg : '')
            + ')',

          'THEN'

        ].join(' '));

      } else {
        // percentages

        const vid = res.getUint8( offset + 2 );
        const verb = verbs[ vid ]
              || (console.warn( 'unknown verb', '0x' + hex(vid) ),'UnknownVerb');



        lines.push('    ' + [

          (op ? '#' + op : '').padEnd(4,' '),

          '  ',

          verb + '('
            + subject
            + (selector ? '.' + selector : '')
            + (arg ? (subject ? ',' : '') + arg : '')
            + ')'

        ].join(' '));

      }

      offset += 8;

    }

    return `NAME "${this.name}"
${lines.join('\n')}`;

  }

}

const health = {
  [0x01]: 'Dead',
  [0x02]: 'Critical',
  [0x03]: 'Serious',
  [0x04]: 'Wounded',
  [0x05]: 'Scratch',
  [0x06]: 'Complete',
};

const flags = {
  [0x01]: 'IsAlive',
  [0x02]: 'IsPoisoned',
  [0x03]: 'IsEnhorsed',
  [0x04]: 'IsAngry',
  [0x05]: 'IsRegen',
  [0x06]: 'IsFear',
  [0x07]: 'IsParalyse',
  [0x08]: 'IsInvisible',
  [0x09]: 'IsXray',
  [0x0a]: 'IsCharmed',
  [0x0b]: 'IsNightVision',
  [0x0c]: 'IsCursed',
  [0x0d]: 'IsBlessed',
  [0x0e]: 'IsConfused',
  [0x0f]: 'IsSleep',
  [0x10]: 'IsLavaProof',
};

// spells
// 10 = Healing
// 14 = Terrorisation
// 18 = Mystic Arrow

/**
 * functions that can be called in an if or if-not
 **/
const predicates = {
  [0x01]: 'True',
  [0x02]: 'False',
  [0x03]: 'IsSpecies',
  [0x04]: 'IsA',
  [0x05]: 'HealthIs',
  [0x06]: 'HealthAtLeast',
  [0x07]: 'HealthLessThan',
  [0x08]: 'InParty',
  [0x09]: 'BeenMet',
  [0x0a]: 'InRange',
  [0x0b]: 'Random',
  [0x0c]: 'TestFlag',
  [0x0d]: 'IsTarget',
  [0x0e]: 'Exists',
  [0x0f]: 'ManaAtLeast',
  [0x10]: 'InLOS',
  [0x81]: 'HasSpell',
  [0x82]: 'HasMeleeWeapon',
  [0x83]: 'HasRangedWeapon',
  [0x84]: 'UsingMeleeWeapon',
  [0x85]: 'UsingRangedWeapon',
  [0x86]: 'OutOfAmmo',
};

/**
 * actions to be performed
 **/
const verbs = {
  [0x01]: 'Continue',
  [0x02]: 'Debug',
  [0x81]: 'CastSpell',
  [0x82]: 'SetTarget',
  [0x83]: 'Retreat',
  [0x84]: 'Pass',
  [0x85]: 'WalkTo',
  [0x86]: 'BattleCry',
  [0x87]: 'EquipMelee',
  [0x88]: 'EquipRanged',
  [0x89]: 'RunAwayFrom',
  [0x8a]: 'RunToward',
  [0x8b]: 'DoAttack',
  [0x8c]: 'FinishCombat',
  [0x8d]: 'SetProtecting',
};

const subjects = {
  [0x01]: 'good',
  [0x02]: 'evil',
  [0x03]: 'neutral',
  [0x04]: 'feral',
  [0x05]: 'ally',
  [0x06]: 'enemy',
  [0x07]: 'bystander',
  [0x08]: 'everybody',
  [0xf0]: 'myself',
  [0xf1]: 'main',
  [0xf2]: 'target',
  [0xf3]: 'last',
};

const selectors = {
  [0x01]: 'CurTarget',
  [0x02]: 'Allies',
  [0x03]: 'Enemies',
  [0x04]: 'Attackers',
  [0x05]: 'ByRange',
  [0x06]: 'ByDamage',
  [0x08]: 'ByStrongest',
  [0x09]: 'ByWeakest',
  [0x0a]: 'Randomize',
  [0x0b]: 'Best',
  [0x0c]: 'Random',
  [0x0d]: 'Pick',
  [0x0e]: 'Protecting',
  [0x0f]: 'ProtectedBy',
  [0x10]: 'LastAttacker',
};

import DataFile from './DataFile.js';
import Character from './Character.js';

/**
 *
 **/
export default class Scenario extends DataFile {

  constructor (file, globals) {

    super( file );

    this.eventTarget = new EventTarget();
    this.globals = Object.assign( globals, {
      stack: [],
      emit: (typeArg,data) => {
        console.log( 'emit', typeArg, data );
        return new Promise( resume => {
          this.eventTarget.dispatchEvent(
            new CustomEvent( typeArg, { detail: { data, resume } } ) );
        } );
      },
      state: new Map(),
      state_flag: new Map(),
    } );

    this.characters = [...Array(256).keys()].map(
      i => new Character(this, i) );

  }

  addEventListener (type, listener, options) {
    return this.eventTarget.addEventListener( type, listener, options );
  }

  removeEventListener (type, listener, options) {
    return this.eventTarget.removeEventListener( type, listener, options );
  }

  get master_index_offset () {
    return this.getUint32( 0x80 );
  }

  get master_index_size () {
    return this.getUint32( 0x84 );
  }

  get ai_scripts () {
    return this.data[3];
  }

  get zone_scripts () {
    return this.data[19];
  }

  get character_scripts () {
    return this.data[23];
  }

  get zones () {
    return this.data[127];
  }

  get zoneports () {
    return new Proxy( [], {
      get: (target, i) => {
        const res = this.getResource( 0xf00c );
        const zone = res.getUint8( i*4 );
        const [x,y] = res.getPair12( i*4 + 1 );
        return { zone, x, y };
      }
    } );
  }

  get prop_lists () {
    return this.data[128];
  }

  get landscapes () {
    return this.data[131];
  }

  get portraits () {
    return this.data[135];
  }

  get skill_icons () {
    return this.data[137];
  }

  get tilesheets () {
    return this.data[141];
  }

  get music () {
    return this.data[143];
  }

  get sounds () {
    return this.data[144];
  }

  get prop_tiles () {
    return new Proxy( [], {
      get: (target, i) => this.getResource( 0xf000 ).getShort( i|0 )
    } );
  }

  get tile_attributes () {
    return new Proxy( [], {
      get: (target, i) => this.getResource( 0xf002 ).getAttribute( i|0 )
    } );
  }

  get tile_names () {
    return new Proxy( [], {
      get: (target, i) => this.getResource( 0xf004 ).getName( i|0 )
    } );
  }

  get schedules () {
    return this.getResource( 0xf00b ).schedules;
  }

  get faux_props () {
    return new Proxy( [], {
      get: (target, i) => this.getResource( 0xf010 ).getFauxProp( i|0 )
    } );
  }

  get xoffsets () {
    return new Proxy( [], {
      get: (target, i) => this.getResource( 0xf011 ).getUint8( i|0 )
    } );
  }

  get yoffsets () {
    return new Proxy( [], {
      get: (target, i) => this.getResource( 0xf012 ).getUint8( i|0 )
    } );
  }

  get tile_compositions () {
    return new Proxy( [], {
      get: (target, i) => this.getResource( 0xf013 ).getComposition( i|0 )
    } );
  }

}

import Binary from './Binary.js';
import Resource, { resid, hex } from './Resource.js';
import makeScript from './Script.js';
import AiScript from './AiScript.js';
import Zone from './Zone.js';
import Landscape from './image/Landscape.js';
import Portrait from './image/Portrait.js';
import SkillIcon from './image/SkillIcon.js';
import TileSheet from './image/TileSheet.js';
import DelvImage from './image/DelvImage.js';
import Music from './Music.js';
import Sound from './Sound.js';
import TileAttributesList from './TileAttributesList.js';
import TileNameList from './TileNameList.js';
import ScheduleList from './ScheduleList.js';
import FauxPropsList from './FauxPropsList.js';
import TileCompositionList from './TileCompositionList.js';

const encryptedResources = [1,2,4,7,8,9,10,11,12,13,14,15,16,19,20,23,24,25,26,27,29,47];

/**
 *
 **/
export default class DataFile extends Binary {

  constructor (file) {
    super(file);

    this.data = this._array_proxy(
      this.master_index_offset + 8,
      this.master_index_size,
      (byteOffset, byteLength, i) => {
        console.log( `%c_array_proxy( offset=0x${hex( byteOffset, 4 )}, length=${byteLength}, i=${i} )`, 'color:#aaa' );
        return this._array_proxy(
          byteOffset,
          byteLength,
          (res_offset, res_size, j) => {
            console.log( `%cresource offset = 0x${hex( res_offset, 4 )}, length = ${res_size}, i = ${i}, j = ${j}`, 'color:#aaa' );
            const encrypted = encryptedResources.includes(i);
            const id = resid( i, j );
            const isScript = i < 127 && !(CLASS_HINTS[i] || CLASS_HINTS[id]);
            const opts = { id, i, j, encrypted };
            if (isScript) {
              return makeScript( this, {...opts, res_offset, res_size } );
            } else {
              return new (CLASS_HINTS[id] || CLASS_HINTS[i] || Resource)(
                this, res_offset, res_size, opts );
            }
          },
          { byteOffset, byteLength } );
      } );
  }

  /**
   * proxy for an array-like object
   * used for accessing the master index and subindices
   *
   * @param {Integer} off - offset into the binary
   * @param {Integer} size - size of the data we're proxying
   * @param {Function} ret - function that defines how to mutate the binary into a return object
   * @param {Object=} keys - keys to return verbatim, also a cache for computed values
   **/
  _array_proxy (off, size, ret, keys={}) {

    const len = size / 8 - 1;

    return new Proxy( [], {
      get: (target, i) => {

        if (i === 'map') {
          return (callback,thisArg) => {
            const acc = new Array( len );
            for (let i = 0; i < size; ++i) {

              const pair = this.getPair32( off + (i * 8) );

              if (pair[0]) {
                acc[i] = callback( ret( pair[0], pair[1], i ), i );
              } else {
                acc[i] = callback( null, i );
              }

            }
            return acc;
          };
        }

        if (keys[ i ]) return keys[ i ];

        i = i|0;

        if (i < 0 || i > size) {
          return undefined;
        }

        const pair = this.getPair32( off + (i * 8) );

        if (pair[0]) {
          return (keys[i] = ret( pair[0], pair[1], i ));
        } else {
          return (keys[i] = null);
        }

      }
    } );
  }

  get title () {
    return this.getPascalString( 0 );
  }

  getResource (resid) {
    const [i,j] = Array.isArray( resid )
          ? resid
          : [ ((resid & 0xff00) >>> 8) - 1, resid & 0xff ];
    return this.data[i][j];
  }

}

/**
 * maps entry in the master index to class constructor
 **/
const CLASS_HINTS = {

  // referenced by position in master index
  3: AiScript,

  127: Zone,
  131: Landscape,
  135: Portrait,
  137: SkillIcon,
  141: TileSheet,
  142: DelvImage,
  143: Music,
  144: Sound,

  // referenced by resource id (resid)
  // 0x0501 is player starting stats and skills
  0x8eff: DelvImage,
  //0xc0d3: Dictionary,
  0xf002: TileAttributesList,
  0xf004: TileNameList,
  0xf00b: ScheduleList,
  0xf010: FauxPropsList,
  0xf013: TileCompositionList,
};

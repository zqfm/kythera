export default class Binary extends DataView {

  constructor (buffer, byteOffset=0, byteLength=undefined) {
    if (buffer instanceof Binary) {
      if (typeof byteLength === undefined) {
        byteLength = buffer.byteLength - byteOffset;
      } else if (byteLength > buffer.byteLength - byteOffset) {
        throw new RangeError( 'invalid binary length' );
      }
      super( buffer.buffer, buffer.byteOffset + byteOffset, byteLength );
    } else {
      super( buffer, byteOffset, byteLength );
    }
  }

  /**
   * Read a Pascal style string
   **/
  getPascalString (offset=0) {
    const len = this.getUint8( offset );
    return new TextDecoder('utf-8')
      .decode( new Uint8Array( this.buffer, this.byteOffset + offset + 1, len ) );
  }

  /**
   * Read a C style NUL terminated string
   **/
  getCString (offset=0) {
    let str = '';
    while (true) {
      const b = this.getUint8( offset++ );
      if (b === 0) {
        return str;
      } else {
        str += String.fromCharCode( b );
      }
    }
  }

  /**
   * Read a pair of uint32s (used as offset and size)
   **/
  getPair32 (offset=0) {
    return [ this.getUint32( offset ), this.getUint32( offset + 4 ) ];
  }

  /**
   * Read a packed pair of uint12s (as used in prop-lists and schedules)
   **/
  getPair12 (offset=0) {
    const [a,b,c] = this.getBytes( offset, 3 );
    return [ (a << 4) | (b >>> 4), ((b & 0xf) << 8) | c ];
  }

  /**
   * Read length bytes starting at offset and return a byte array
   **/
  getBytes (offset=0, length=this.byteLength) {
    return new Uint8Array( this.buffer, this.byteOffset + offset, length );
  }

  /**
   * Return a Uint16Array with the correct endianess
   **/
  getShorts (offset=0, length=this.byteLength/2) {
    const self = this;
    return new Uint16Array( function*() {
      for (let i = 0; i < length; ++i) {
        yield self.getUint16( offset + (i * 2) );
      }
    }() );
  }

  /**
   * Get the nth Uint16 rather than a Uint16 by offset
   **/
  getShort (n=0) {
    return this.getUint16( n * 2 );
  }

  /**
   * Read a 24 bit unsigned integer
   **/
  getUint24 (offset) {
    const [a,b,c] = this.getBytes( offset, 3 );
    return a << 16 | b << 8 | c;
  }

}

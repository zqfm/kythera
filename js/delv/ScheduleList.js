import Resource from './Resource.js';

/**
 * https://github.com/BryceSchroeder/delvmod/wiki/Schedules
 *
 * Header is 0x100 uint16s indicating the number of schedules per character
 * Starting at offset 0x200 are the schedules themselves
 * which are 8 bytes in the following format
 * byte0   = hour at which the schedule begins
 * byte1   = behaviour (sets character field 0x15)
 * byte2-3 = not sure yet
 * byte4   = zone
 * xy24    = either starting or destination coords in the zone
 **/
export default class ScheduleList {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {
    const res = this.resource = new Resource( src, byteOffset, byteLength, opts );
    this.schedules = [];

    if (this.schedules.length === 0) {
      const sizes = [];
      for (let i = 0; i < 0x100; ++i) {
        sizes.push( res.getUint16( i * 2 ) );
      }
      let cursor = 0x200;
      for (let i = 0; i < 0x100; ++i) {
        const schedules = [];
        for (let j = 0; j < sizes[i]; ++j) {
          const [x,y] = res.getPair12( cursor + 5 );
          schedules.push({
            hour: res.getUint8( cursor ),
            behavior: res.getUint8( cursor + 1 ),
            flags: res.getUint16( cursor + 2 ),
            zone: res.getUint8( cursor + 4 ),
            x,
            y,
          });
          cursor += 8;
        }
        this.schedules.push( schedules );
      }
    }
  }

  getSchedule (i=0) {
    return this.schedules[i];
  }

}

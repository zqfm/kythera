import Resource from './Resource.js';

export default class Sound {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {
    const res = this.resource = new Resource( src, byteOffset, byteLength, opts );
    this.magic = new TextDecoder('utf-8').decode( res.getBytes(0,4) );
    console.assert( this.magic === 'asnd', `Bad magic number, expecting "asnd" but got "${this.magic}"` );

    this.duration = res.getUint32(4);
    this.audioCtx = new AudioContext({ sampleRate: res.getUint16(8) });
    this.flags = res.getUint16(10);
  }

  get buffer () {
    if (!this._buffer) {
      const res = this.resource;
      let offset = 12, i = 0;
      const audioBuffer = this._buffer = this.audioCtx.createBuffer( 1, this.length, this.audioCtx.sampleRate );
      const channel = audioBuffer.getChannelData(0);
      while (offset < res.byteLength) {
        channel[i++] = (res.getInt16( offset ) - 127) / 128;
        offset += 2;
      }
    }
    return this._buffer;
  }

  get length () {
    return (this.resource.byteLength - 12 >>> 1) + 1;
  }

  set dirty (val) {
    if (val) {
      this._samples = null;
    }
  }

  play () {
    const source = this.audioCtx.createBufferSource();
    const gain = this.audioCtx.createGain();
    source.connect( gain );
    gain.connect( this.audioCtx.destination );
    source.buffer = this.buffer;
    gain.gain.value = 0;
    setTimeout( () => gain.gain.value = 0.5, 15 );
    setTimeout( () => gain.gain.value = 0, ((this.length / this.audioCtx.sampleRate) * 1000) - 15 );
    source.start(0);
  }

}

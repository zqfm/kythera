import Resource from './Resource.js';

/**
 * Tile names are stored as a non-random access list of a 16 bit "cutoff" followed by a C string
 * terminated with \0, then the next cutoff and C string, etc. Because it's not random access, we
 * build up a random access cache of cutoffs and the names.
 *
 * [ 2 byte cutoff, NUL terminated string, 2 byte cutoff, NUL terminated string, ... ]
 *
 * The cutoffs represent a range from the previous cutoff (exclusive) to the current one (inclusive)
 * such that the index of the current cutoff is the index into the array of names. Because I'm sure
 * that makes litte sense, here's a sample from the first few entries for illustration:
 *
 * cutoffs = [         0,       1,       4,       5,      7,      15, ... ]
 * names   = [ "Nothing", "grass", "swamp", "shrub", "bush", "water", ... ]
 *
 * What this translates to is this:
 *
 * Tile 0 is named "Nothing"
 * Tile 1 is named "grass"
 * Tiles 2 - 4 are named "swamp"
 * Tile 5 is named "shrub"
 * Tiles 6 and 7 are named "bush"
 * and Tiles 8 - 15 are named "water"
 *
 **/

export default class TileNameList {

  constructor (src, byteOffset=0, byteLength=0, opts={}) {
    const res = this.resource = new Resource( src, byteOffset, byteLength, opts );
    this.cutoffs = [];
    this.names = [];

    if (this.names.length === 0) {
      let cursor = 0;
      while (cursor < res.byteLength) {
        const cutoff = res.getUint16( cursor );
        cursor += 2;
        const name = res.getCString( cursor );
        if (!name && !cutoff) {
          break;
        }
        cursor += name.length + 1;
        this.cutoffs.push( cutoff );
        this.names.push( name );
      }
    }
  }

  getName (tile=0) {
    return this.names[ this.cutoffs.findIndex( cutoff => tile <= cutoff ) ];
  }

}

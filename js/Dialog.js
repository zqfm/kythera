const H = ReactDOMFactories;
const { useEffect, useState } = React;
import { GLOBAL } from './delv/constants.js';

function printf (format,...args) {
  let n = 0;
  while (/%s/.test( format ) && n < args.length) {
    format = format.replace( /%s/, args[n++] || '%s' );
  }
  return format;
}

export default function Dialog ({
  scenario,
  current,
  text,
  keywords=[],
  participants=[],
  menu,
  resume,
  expectResponse,
  willFinish=false, // keeps the last line of dialog open
  n=0 })
{

  const [line,setLine] = useState();
  const [input,setInput] = useState('');
  const [used,setUsed] = useState([]);

  useEffect( () => {

    participants.forEach( (id,slot) => {
      console.log( 'draw participant', id, slot );
      if (id) {
        const ch = scenario.characters[id];
        const canvas = document.querySelector( `#dialog .participant:nth-of-type(${slot+1}) canvas` );
        if (ch.portrait && canvas) {
          ch.portrait.draw( canvas.getContext('2d') );
        }
      }
    } );

  }, [...participants] );

  const lines = (text || '').split('*').filter( line => line !== '*' );
  const quote = /^"/.test( lines[line] );

  useEffect( () => {
    setLine(0);
    if (text && lines.length === 1) {
      //resume();
    }
  }, [text,n] );

  const note = H.button(
    { key: 'note',
      className:'button take-note',
      onClick: e => {
        e.stopPropagation();
      } });

  return H.div(
    { id: 'dialog',
      className: line < lines.length - 1 ? 'more' : '',
      onClick: e => {
        if (e.currentTarget.nodeName === 'A') { return; }
        if (line < lines.length - 1) {
          setLine( line + 1 );
        }
        if ((line + 1) === (lines.length - (willFinish ? 0 : 1))) {
          resume();
        }
      } },

    participants.map(
      (p,i) => H.div(
        { key: `${p}-${i}`, className: 'participant' },
        p ? H.figure(
          {},
          H.canvas({ width: 64, height: 64 }),
          H.figcaption({}, scenario.characters[p].name) ) : null,
        !menu ? H.p(
          { className: 'line' },
          expectResponse && p === 1 ? [...keywords, 'Where Is...', 'Job', 'Name', 'Bye', '']
            .filter( key => !used.includes( key ) )
            .map( key => H.span(
              { key },
              ' - ',
              key === ''
                ? H.input({
                  type: 'text',
                  value: input,
                  onChange: e => setInput( e.currentTarget.value ),
                  onKeyDown: e => {
                    if (e.key === 'Enter') {
                      e.preventDefault();
                      scenario.globals[GLOBAL.CONVERSATION_RESPONSE] = input;
                      setInput('');
                      resume();
                    }
                  } })
                : H.a({ key, href: '#', onClick: e => {
                  e.preventDefault();
                  e.stopPropagation();
                  console.log( 'conversation-response', key );
                  scenario.globals[GLOBAL.CONVERSATION_RESPONSE] = key;
                  setUsed([ ...used, key ]);
                  resume();
                } }, key)) )
            : quote && p === current
            ? [
              lines[line]
                .replace( /\^Name/g, scenario.globals[GLOBAL.PLAYER_NAME] )
                .match( /([^@]+|@[A-Za-z]+)/g )
                .map( part => {
                  if (part.substr(0,1) === '@') {
                    return H.span({ key: part, className: 'keyword' }, part.substr(1));
                  } else {
                    return part;
                  }
                } ),
              note]
          : '' )
          : null ) ),

    menu ? H.div(
      { className: 'menu' },
      menu.prompt,
      H.ul(
        { className: 'items' },
        menu.items.map(
          (item,i) => H.li(
            { key: item },
            H.a(
              { href: '#',
                onClick: e => {
                  e.preventDefault();
                  e.stopPropagation();
                  resume(i);
                }
              },
              printf( menu.format, ...item ) ) ) ) ),
      H.ul(
        { className: 'buttons' },
        menu.buttons.map(
          button => H.li(
            { key: button },
            H.button(
              { className: 'button',
                type: 'button',
                onClick: e => {
                  e.preventDefault();
                  e.stopPropagation();
                  if (button === 'Cancel') {
                    resume(-1);
                  } else {
                    console.warn( `unimplemented button ${button}` );
                  }
                } },
              button ) ) ) ) )
      : null,

    H.p(
      { className: 'desc' },
      quote || menu ? null : [lines[line],note]) );

}

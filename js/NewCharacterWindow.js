const H = ReactDOMFactories;
const { createElement: c, useEffect, useRef, useState } = React;

import { GLOBAL } from './delv/constants.js';
import Character from './delv/Character.js';
import Window from './Window.js';

export default function NewCharacterWindow (props) {

  const portraitCvs = useRef(null);
  const { scenario } = props;

  const archetypes   = scenario.data[1][0x03];
  const descriptions = scenario.data[1][0x04];
  const stats        = scenario.data[1][0x05];
  const skills       = scenario.data[1][0x06];

  const [archetype,setArchetype] = useState(0);
  const [gender,setGender] = useState(0);
  const [portrait,setPortrait] = useState(0);

  const drawPortrait = (pidx,gender) => {
    const i = (gender ? 0xf5 : 0xef) + pidx;
    const portrait = scenario.portraits[i];
    if (portrait && portraitCvs.current) {
      portrait.draw( portraitCvs.current.getContext('2d') );
    }
  };

  useEffect( () => {
    drawPortrait( portrait, gender );
  }, [] );

  return c(
    Window,
    { ...props,
      width: 385,
      height: 403,
      className: 'new-character alt' },
    H.div(
      { className: 'archetype-wrap' },
      H.h2({}, 'Character Archetype'),
      H.div(
        { className: 'archetypes' },
        archetypes.map(
          (arch,i) => H.div(
            { key: i,
              className: `option ${archetype === i ? 'selected' : ''}`,
              onClick: e => setArchetype(i) },
            arch ) ) ),

      H.p({}, descriptions.get( archetype )) ),

    H.div(
      { className: 'right' },
      H.h2({}, 'Select'),
      H.h2({}, 'Portrait'),
      H.div(
        { className: 'portrait' },
        H.canvas({ width: 64, height: 64, ref: portraitCvs }),
        H.div(
          { className: 'scrollbar' },
          H.button({
            className: 'up',
            onClick: e => {
              const p = Math.max( portrait - 1, 0 );
              drawPortrait( p, gender );
              setPortrait(p);
            } }),
          H.div(
            { className: 'track' },
            H.button({ className: 'thumb',
                       style: { top: ((portrait - 1) * 8) + 'px' } }) ),
          H.button({
            className: 'down',
            onClick: e => {
              const p = Math.min( portrait + 1, 2 );
              drawPortrait( p, gender );
              setPortrait(p);
            } }),
        ) ),
      H.div(
        {},
        H.label(
          {},
          H.input({
            type: 'radio',
            name: 'gender',
            checked: gender === 0,
            onChange: e => {
              drawPortrait( portrait, 0 );
              setGender(0);
            }
          }),
          H.span({},'Male')),
        H.label(
          {},
          H.input({
            type: 'radio',
            name: 'gender',
            checked: gender === 1,
            onChange: e => {
              drawPortrait( portrait, 1 );
              setGender(1);
            }
          }),
          H.span({},'Female')) ) ),

    H.dl(
      {},
      H.dt({}, 'Attributes'),
      H.dd({}, stats.get( archetype )),
      H.dt({}, 'Aptitudes'),
      H.dd({}, skills.get( archetype )) ),

    H.div(
      {},
      H.button(
        { className: 'button',
          onClick: e => props.close() },
        'Cancel'),
      H.button(
        { className: 'button',
          onClick: e => props.close({ gender, archetype, portrait }) },
        'OK' ) ) );
}

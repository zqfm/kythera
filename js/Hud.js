const { useState, useRef, useEffect, createElement: c } = React;
const H = ReactDOMFactories;

function Popup (props) {

  const [height, setHeight] = useState(0);
  const [dragging, setDragging] = useState(false);

  return H.div(
    { className: `popup ${props.className}`,
      style: { height: `${height + 16}px` } },
    H.header(
      { onMouseDown: e => {
        if (!dragging && e.button === 0) {
          e.preventDefault();
          const offsetY = height + e.pageY;
          setDragging( true );
          const drag = e => {
            e.preventDefault();
            setHeight( Math.max( offsetY - e.pageY, 0 ) );
          };
          document.addEventListener( 'mousemove', drag );
          document.addEventListener( 'mouseup', () => {
            setDragging( false );
            document.removeEventListener( 'mousemove', drag );
          }, { once: true } );
        }
      } },
      H.h1({}, H.span({}, props.title)) ),
    H.div(
      { className: 'content' },
      props.children ) );

}

function PartyMember ({ m, scenario }) {

  const canvas = useRef(null);
  let char = scenario.characters[m];

  useEffect( () => {
    const ctx = canvas.current.getContext('2d');
    char = scenario.characters[m];
    char.draw( ctx, 0, 0, { aspect: 9 } );
  }, [m] );

  return H.div(
    { className: 'member', key: char.name },
    H.button(
      { type: 'button', className: 'occupied' },
      H.canvas({ ref: canvas, width: 32, height: 32 }),
      H.progress({ max: char.full_health, value: char.health }) ),
    H.select(
      { className: 'perform' },
      H.option( {}, 'Attack [a]' ),
      H.option( {}, 'Look [l]' ),
      H.option( {}, 'Divide Food [f]' ),
      H.option( {}, 'Estimate Time [e]' ) ) );
}

export default function Hud (props) {

  const { scenario } = props;

  const canvas = useRef(null);

  const drawLandscape = i => {
    if (canvas.current) {
      const ctx = canvas.current.getContext('2d');
      const landscape = scenario.landscapes[ i ];
      if (landscape) {
        landscape.drawBackground( ctx, 0 );
        landscape.draw( ctx );
      }
    }
  };

  useEffect( () => {
    if (scenario) {
      scenario.addEventListener( 'set-landscape-image', e => {
        drawLandscape( e.detail.data );
      } );
    }
  }, [scenario] );

  return H.div(
    { id: 'hud' },
    H.div(
      { className: 'popups' },
      c( Popup, { className: 'quest', title: 'To Do' },
         H.ul({}, (props.quests || []).map(
           quest => H.li(
             { key: quest.name },
             H.label(
               {},
               H.input({ type: 'checkbox', disabled: true, checked: quest.done }),
               H.span({}, quest.name) ) ) ) ) ),
      c( Popup, { className: 'journal', title: 'Journal' },
         H.ul({}, (props.journal || []).map(
           entry => H.li(
             { key: entry.id },
             entry.text ) ) ) ) ),
    H.div(
      { className: 'content' },
      H.div({ className: 'hotkeys' }),
      H.div(
        { className: 'log-wrap mini-border' },
        H.header(
          {},
          H.button({ className: 'action ' + props.action }),
          H.h1({ className: 'looking-at' }, H.span({}, props.lookingAt || '')) ),
        H.textarea({ className: 'log', readOnly: true, value: props.logText }) ),
      H.div(
        { className: 'landscape-party-wrap' },
        H.canvas({ className: 'landscape mini-border', ref: canvas, width: 288, height: 32 }),
        H.div( { className: 'party mini-border' },
               props.party.map( m => c( PartyMember, { m, scenario } ) ) ) ) ) );

}

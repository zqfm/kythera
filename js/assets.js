import Tile from './delv/Tile.js';

export default function assets (scenario) {

  const s = [
    `html { background: url(${scenario.getResource( 0x8f00 ).canvas.toDataURL()}) repeat; }`,
    `#hud > .content:before { border-image: url(${scenario.getResource( 0x8f80 ).canvas.toDataURL()}) 32 32 fill repeat; }`,
    `#hud .hotkeys { background: url(${scenario.getResource( 0x8f81 ).canvas.toDataURL()}) center; }`
  ];

  // tilesheets
  const ts0 = new Tile( scenario, 0x180 ).tilesheet.canvas;
  const ts1 = new Tile( scenario, 0x190 ).tilesheet.canvas;
  const ts2 = new Tile( scenario, 0x1a0 ).tilesheet.canvas;

  // window border
  const cvs48 = document.createElement('canvas');
  cvs48.width = cvs48.height = 48;
  const ctx48 = cvs48.getContext('2d');

  ctx48.drawImage( ts2, 0 * 16, 4 * 16, 16, 16, 0 * 16, 0 * 16, 16, 16 );
  ctx48.drawImage( ts2, 0 * 16, 0 * 16, 16, 16, 1 * 16, 0 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 4 * 16, 16, 16, 2 * 16, 0 * 16, 16, 16 );

  ctx48.drawImage( ts2, 0 * 16, 2 * 16, 16, 16, 0 * 16, 1 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 2 * 16, 16, 16, 2 * 16, 1 * 16, 16, 16 );

  ctx48.drawImage( ts2, 0 * 16, 5 * 16, 16, 16, 0 * 16, 2 * 16, 16, 16 );
  ctx48.drawImage( ts2, 0 * 16, 1 * 16, 16, 16, 1 * 16, 2 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 5 * 16, 16, 16, 2 * 16, 2 * 16, 16, 16 );

  s.push( `.window { border-image: url(${cvs48.toDataURL()}) 16 16 repeat; }` );

  // alternate window border
  ctx48.clearRect(0,0,48,48);

  ctx48.drawImage( ts2, 0 * 16, 4 * 16, 16, 16, 0 * 16, 0 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 0 * 16, 16, 16, 1 * 16, 0 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 4 * 16, 16, 16, 2 * 16, 0 * 16, 16, 16 );

  ctx48.drawImage( ts2, 0 * 16, 3 * 16, 16, 16, 0 * 16, 1 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 3 * 16, 16, 16, 2 * 16, 1 * 16, 16, 16 );

  ctx48.drawImage( ts2, 0 * 16, 5 * 16, 16, 16, 0 * 16, 2 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 1 * 16, 16, 16, 1 * 16, 2 * 16, 16, 16 );
  ctx48.drawImage( ts2, 1 * 16, 5 * 16, 16, 16, 2 * 16, 2 * 16, 16, 16 );

  s.push( `.window.alt { border-image: url(${cvs48.toDataURL()}) 16 16 repeat; }` );

  // window background
  const cvs32 = document.createElement('canvas');
  cvs32.width = cvs32.height = 32;
  const ctx32 = cvs32.getContext('2d');
  ctx32.drawImage( ts2, 0, 4 * 32, 32, 32, 0, 0, 32, 32 );

  s.push( `.window>.content { background: url(${cvs32.toDataURL()}) repeat; }` );

  // window resize
  const cvs16 = document.createElement('canvas');
  cvs16.width = cvs16.height = 16;
  const ctx16 = cvs16.getContext('2d');
  ctx16.drawImage( ts2, 16, 15 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.window .resize { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // window close
  ctx16.clearRect(0,0,16,16);
  ctx16.drawImage( ts2, 0, 14 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.window .close { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // window close active
  ctx16.clearRect(0,0,16,16);
  ctx16.drawImage( ts2, 16, 14 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.window .close:active { background: url(${cvs16.toDataURL()}) no-repeat; }`);

  // mini border
  const mbCanvas = document.createElement('canvas');
  mbCanvas.width = 54; mbCanvas.height = 118;
  const mbContext = mbCanvas.getContext('2d');
  mbContext.drawImage( scenario.getResource( 0x8f83 ).canvas, 5, 5, 54, 118, 0, 0, 54, 118 );
  s.push( `.mini-border { border-image: url(${mbCanvas.toDataURL()}) 3 3 repeat; }` );

  // popup headers
  const cvs32_16 = document.createElement('canvas');
  cvs32_16.width = 32; cvs32_16.height = 16;
  const ctx32_16 = cvs32_16.getContext('2d');
  ctx32_16.drawImage( ts1, 0, 12 * 32, 32, 16, 0, 0, 32, 16 );
  s.push( `.popup { border-image: url(${cvs32_16.toDataURL()}) 16 8 0 repeat; }` );

  // window title
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 25 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `header h1 { border-image: url(${cvs32_16.toDataURL()}) 8 8 repeat; }` );

  // dialog background
  const ts8d0 = new Tile( scenario, 0x8d0 ).tilesheet.canvas;
  const cvs2 = document.createElement('canvas');
  cvs2.width = cvs2.height = 2;
  const ctx2 = cvs2.getContext('2d');
  ctx2.drawImage( ts8d0, 0, 15 * 32, 2, 2, 0, 0, 2, 2 );
  s.push( `#dialog { background: url(${cvs2.toDataURL()}) repeat; }` );

  // dialog border
  ctx32.clearRect( 0, 0, 32, 32 );
  ctx32.drawImage( ts1, 0, 13 * 32, 32, 32, 0, 0, 32, 32 );
  s.push( `#dialog { border-image: url(${cvs32.toDataURL()}) 4 8 repeat; }` );

  // radio button
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 0, 24 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `input[type=radio] + span { background: url(${cvs16.toDataURL()}) 0 center no-repeat; }` );

  // radio button checked
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 16, 24 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `input[type=radio]:checked + span { background: url(${cvs16.toDataURL()}) 0 center no-repeat; }` );

  // radio button disabled
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 0, 25 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `input[type=radio disabled] + span { background: url(${cvs16.toDataURL()}) 0 center no-repeat; }` );

  // button
  ctx32.clearRect( 0, 0, 32, 32 );
  ctx32.drawImage( ts2, 0, 8 * 32, 32, 32, 0, 0, 32, 32 );
  s.push( `.button { border-image: url(${cvs32.toDataURL()}) 6 fill repeat; }` );

  // button active
  ctx32.clearRect( 0, 0, 32, 32 );
  ctx32.drawImage( ts2, 0, 9 * 32, 32, 32, 0, 0, 32, 32 );
  s.push( `.button:active { border-image: url(${cvs32.toDataURL()}) 6 fill repeat; }` );

  // button disabled
  ctx32.clearRect( 0, 0, 32, 32 );
  ctx32.drawImage( ts2, 0, 10 * 32, 32, 32, 0, 0, 32, 32 );
  s.push( `.button[disabled] { border-image: url(${cvs32.toDataURL()}) 6 fill repeat; }` );

  // scroll up
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 0, 26 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.scrollbar .up { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // scroll down
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 0, 27 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.scrollbar .down { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // scroll up active
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 0, 28 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.scrollbar .up:active { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // scroll down active
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 0, 29 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.scrollbar .down:active { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // scrollbar thumb
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 16, 28 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `.scrollbar .thumb { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // scrollbar track
  const cvs16_32 = document.createElement('canvas');
  cvs16_32.width = 16; cvs16_32.height = 32;
  const ctx16_32 = cvs16_32.getContext('2d');
  ctx16_32.drawImage( ts2, 16, 26 * 16, 16, 32, 0, 0, 16, 32 );
  s.push( `.scrollbar .track { border-image: url(${cvs16_32.toDataURL()}) 8 repeat; }` );

  // "do" button
  const cvs34_22 = document.createElement('canvas');
  cvs34_22.width = 34; cvs34_22.height = 22;
  const ctx34_22 = cvs34_22.getContext('2d');
  const perform = scenario.getResource( 0x8f86 ).canvas;
  ctx34_22.drawImage( perform, 1, 1, 34, 22, 0, 0, 34, 22 );
  s.push( `select.perform { background: url(${cvs34_22.toDataURL()}) no-repeat; }` );

  ctx34_22.drawImage( perform, 1, 25, 34, 22, 0, 0, 34, 22 );
  s.push( `select.perform:active { background: url(${cvs34_22.toDataURL()}) no-repeat; }` );

  // party background
  const cvs288_75 = document.createElement('canvas');
  cvs288_75.width = 288; cvs288_75.height = 75;
  const ctx288_75 = cvs288_75.getContext('2d');
  const party = scenario.getResource( 0x8f84 ).canvas;
  ctx288_75.drawImage( party, 8, 46, 288, 75, 0, 0, 288, 75 );
  s.push( `#hud .party { background: url(${cvs288_75.toDataURL()}) no-repeat; }` );

  // party member button
  const cvs36_50 = document.createElement('canvas');
  cvs36_50.width = 36; cvs36_50.height = 50;
  const ctx36_50 = cvs36_50.getContext('2d');
  const occupied = scenario.getResource( 0x8f85 ).canvas;
  ctx36_50.drawImage( occupied, 0, 0, 36, 50, 0, 0, 36, 50 );
  s.push( `button.occupied { background: url(${cvs36_50.toDataURL()}) no-repeat; }` );

  ctx36_50.drawImage( occupied, 36, 0, 36, 50, 0, 0, 36, 50 );
  s.push( `button.occupied:active { background: url(${cvs36_50.toDataURL()}) no-repeat; }` );

  // action
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 6 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `#hud .action { background: url(${cvs32_16.toDataURL()}) no-repeat; }` );

  // action move
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 0 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `#hud .action.move { background: url(${cvs32_16.toDataURL()}) no-repeat; }` );

  // action attack
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 1 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `#hud .action.attack { background: url(${cvs32_16.toDataURL()}) no-repeat; }` );

  // action talk
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 2 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `#hud .action.talk { background: url(${cvs32_16.toDataURL()}) no-repeat; }` );

  // action use
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 3 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `#hud .action.use { background: url(${cvs32_16.toDataURL()}) no-repeat; }` );

  // action look
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 4 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `#hud .action.look { background: url(${cvs32_16.toDataURL()}) no-repeat; }` );

  // action take
  ctx32_16.clearRect( 0, 0, 32, 16 );
  ctx32_16.drawImage( ts1, 0, 5 * 16, 32, 16, 0, 0, 32, 16 );
  s.push( `#hud .action.take { background: url(${cvs32_16.toDataURL()}) no-repeat; }` );

  // checkbox
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 0, 22 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `input[type="checkbox"] + span { background: url(${cvs16.toDataURL()}) no-repeat; }` );

  // checkbox checked
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts2, 16, 22 * 16, 16, 16, 0, 0, 16, 16 );
  s.push( `input[type="checkbox"]:checked + span { background: url(${cvs16.toDataURL()}) no-repeat; }` );


  // dialog journal
  ctx16.clearRect( 0, 0, 16, 16 );
  ctx16.drawImage( ts0, 0, 14 * 16, 16, 16, 0, 0, 16, 16 );

  s.push( `#dialog .take-note:before {
  content: "";
  position: absolute;
  top: -6px;
  left: -6px;
  background: url(${cvs16.toDataURL()}) center center no-repeat;
  width: 24px;
  height: 24px;
}` );

  return s.join('\n');
}

import Scenario from './delv/Scenario.js';
import SaveFile from './delv/SaveFile.js';
import { hex } from './delv/Resource.js';
import DelvImage from './delv/image/DelvImage.js';
import Tile from './delv/Tile.js';
import { test } from './test.js';
import Character from './delv/Character.js';
import { GLOBAL } from './delv/constants.js';

import Hud from './Hud.js';
import Dialog from './Dialog.js';
import Window from './Window.js';
import DebugWindow from './DebugWindow.js';
import MainMenu from './MainMenu.js';
import NewCharacterWindow from './NewCharacterWindow.js';
import PlayerNameWindow from './PlayerNameWindow.js';
import WorldWindow from './WorldWindow.js';
import assets from './assets.js';
import randomId from './random-id.js';

const { createElement: c } = React;
const H = ReactDOMFactories;

export default class Cythera extends React.Component {

  constructor (props) {
    super( props );

    this.state = {
      loaded: false,
      cursor: { x: 0, y: 0 },
      currentZone: 0,
      cameraX: 20,
      cameraY: 24,
      drawPropsCheck: true,
      drawFauxPropsCheck: true,
      drawDebugPropsCheck: false,
      drawRoofsCheck: false,
      daySlider: 0,
      timeSlider: 0,
      title: 'Cythera',
      windows: [],
      logText: '',
      dialog: null,
      action: 'move',
      quests: [],
      lookingAt: '',
      scenario: null,
      showHud: false,
      redraw: 0,
      party: [],
    };
    this.dialogBuffer = '';
  }

  componentDidMount () {
    this.setState({ loaded: false });
    fetch( '/Cythera Data' )
      .then( response => response.blob() )
      .then( blob => blob.arrayBuffer() )
      .then( buf => this.onScenarioLoaded( buf ) );

    document.addEventListener( 'keydown', ({ key, target, shiftKey }) => {

      const { action, scenario } = this.state;

      if (target.nodeName === 'INPUT') { return; }

      switch (key) {

      case 'ArrowUp':
        this[action||'move']({ dir: 0, centerCamera: true });
        break;
      case 'ArrowRight':
        this[action||'move']({ dir: 1, centerCamera: true });
        break;
      case 'ArrowDown':
        this[action||'move']({ dir: 2, centerCamera: true });
        break;
      case 'ArrowLeft':
        this[action||'move']({ dir: 3, centerCamera: true });
        break;

      case 'Enter':
        const dialog = document.getElementById('dialog');
        if (dialog) {
          dialog.click();
        }
        break;

      case 'a':
        this.setState({ action: 'attack' }); break;
      case 'f':
        /* divide food */ break;
      case 'g':
        this.setState({ action: 'take' }); break;
      case 'l':
        this.setState({ action: 'look' }); break;
      case 'm':
        this.setState({ action: 'move' }); break;
      case 'p':
        /* pool money */ break;
      case 't':
        this.setState({ action: 'talk' }); break;
      case 'u':
        this.setState({ action: 'use' }); break;
      case 'x':
        this.lookAround(); break;
      }

    } );

  }

  async onScenarioLoaded (arrayBuffer) {

    const globals = {
      visited: {}, // rooms that have been visited
      [GLOBAL.CURRENT_TIME]: 'morning',
      [GLOBAL.CURRENT_ROOM]: 0,
      [GLOBAL.CURRENT_HOUR]: 0,
    };
    const scenario = new Scenario( arrayBuffer, globals );

    const { windows } = this.state;
    windows.push({
      key: 'main-menu',
      type: MainMenu,
      left: (document.body.clientWidth / 2) - 336,
      top: (document.body.clientHeight / 2) - 256,
      scenario,
      registered: null,
      newGame: this.newGame.bind(this),
      onSaveFileLoaded: this.onSaveFileLoaded.bind(this),
    });
    this.setState({ windows, scenario });

    let styles = document.getElementById('cythera-css');
    if (styles) {
      styles.innerText = '';
    } else {
      styles = document.createElement('style');
      styles.id = 'cythera-css';
      document.head.appendChild( styles );
    }
    styles.textContent = assets( scenario );

    scenario.addEventListener( 'print', e => {
      this.print( e.detail.data ).then( e.detail.resume );
    } );

    scenario.addEventListener( 'string', e => {
      this.print( e.detail.data ).then( e.detail.resume );
    } );

    scenario.addEventListener( 'open-conversation', e => {
      this.dialogBuffer = { participants: [0,0,0] };
    });

    // TODO queue up dialog events, eg. participant 0 says "x", participant 1 says "y", etc.
    scenario.addEventListener( 'talk-participant', e => {
      if (this.dialogBuffer) {
        const { slot, participant } = e.detail.data;
        this.dialogBuffer.participants[slot] = participant;
        this.dialogBuffer.current = participant;
      } else {
        console.warn( 'talk-participant called with no dialog!' );
      }
    } );

    scenario.addEventListener( 'add-conversation-keyword', e => {
      if (this.dialogBuffer) {
        const { keywords=[] } = this.dialogBuffer;
        keywords.unshift( e.detail.data );
        this.dialogBuffer.keywords = keywords;
      } else {
        console.warn( 'add-response-keyword called with no dialog!' );
      }
    } );

    scenario.addEventListener( 'done-talking', e => {
      if (this.dialogBuffer) {
        const { keywords=[], n=0 } = this.dialogBuffer;
        this.setState({ dialog: { ...this.dialogBuffer, resume: e.detail.resume } });
        this.dialogBuffer.text = null;
        this.dialogBuffer.n = n+1;
      } else {
        console.warn( 'done-talking called with no dialog!' );
      }
    } );

    scenario.addEventListener( 'conversation-response', e => {
      const { dialog } = this.state;
      if (dialog) {
        this.dialogBuffer.expectResponse = true;
        this.setState({ dialog: { ...dialog, expectResponse: true, resume: e.detail.resume } });
      } else {
        console.warn( 'conversation-response called with no dialog!' );
      }
    } );

    scenario.addEventListener( 'show-menu', e => {
      const { dialog } = this.state;
      if (dialog) {
        this.setState({ dialog: { ...dialog, menu: e.detail.data, resume: e.detail.resume } });
      } else {
        console.warn( 'show-menu called with no dialog!' );
      }
    } );

    scenario.addEventListener( 'finish-conversation', e => {
      this.dialogBuffer = null;
      this.setState({ dialog: null });
    });

    scenario.addEventListener( 'add-quest', e => {
      this.setState({ quests: [...this.state.quests, { name: e.detail.data }] });
    } );

    scenario.addEventListener( 'begin-cutscene', e => {
      if (this.dialogBuffer) {
        const { keywords=[], n=0 } = this.dialogBuffer;
        this.setState({ dialog: {
          ...this.dialogBuffer,
          expectResponse: false,
          resume: e.detail.resume
        } });
        this.dialogBuffer.text = null;
        this.dialogBuffer.n = n+1;
      }
    } );

    scenario.addEventListener( 'begin-slideshow', e => {
      this.setState({ slideshow: {} });
    } );

    scenario.addEventListener( 'slideshow', e => {
      console.log( 'slideshow', e );
      this.setState({ slideshow: { ...e.detail.data, resume: e.detail.resume } });
    } );

    scenario.addEventListener( 'end-slideshow', e => {
      this.setState({ slideshow: null });
    } );

    scenario.addEventListener( 'change-zone', e => {
      this.changeZone( e.detail.data.zoneport );
    } );

    scenario.addEventListener( 'set-title', e => {
      const { data } = e.detail;
      document.querySelector('head title').textContent = data;
      this.setState({ title: data });
    } );

    /*
    if (test( scenario )) {
      console.log( 'scenario is a-ok!' );
    }
    */

    console.log( 'title:', scenario.title );

    Object.assign( window, {

      app: this,

      scenario,

      loadScenario: () => {
        const input = document.createElement('input');
        input.type = 'file';
        input.onchange = async (e) => {

          this.setState({ loaded: false });

          const file = e.currentTarget.files[0];
          const arrayBuffer = await new Promise( resolve => {
            const fr = new FileReader();
            fr.onload = e => resolve( e.target.result );
            fr.readAsArrayBuffer( file );
          } );

          this.onScenarioLoaded( arrayBuffer );

          input.parentNode.removeChild( input );

        };

        document.body.appendChild( input );
        input.click();

      },

      print: str => this.print( str ),

      debug: () => {
        const { windows, currentZone } = this.state;
        windows.push({
          key: 'debug',
          type: DebugWindow,
          scenario,
          currentZone,
          changeZone: zoneport => this.changeZone( zoneport ),
          clickPortrait: async (who) => {
            if (this.state.action === 'talk') {
              scenario.globals.emit( 'open-conversation' );
              scenario.globals.emit( 'talk-participant', { slot: 2, participant: 1 } );
              scenario.globals.emit( 'talk-participant', { slot: 0, participant: who } );

              await scenario.data[23][who].get(12).call( scenario.globals[ GLOBAL.PLAYER_CHARACTER ] );

              if (this.dialogBuffer) {
                this.setState({ dialog: {
                  ...this.dialogBuffer,
                  expectResponse: false,
                  willFinish: true,
                  resume: () => {
                    this.setState({ dialog: null });
                  } } });
                this.dialogBuffer = null;
              }
            }
          },
          close: e => {
            this.setState({ windows: this.state.windows.filter( w => w.key !== 'debug' ) });
          },
          setState: (state,callback) => this.setState(state, callback),
        });
        this.setState({ windows });
      },

    } );

  }

  async onSaveFileLoaded (arrayBuffer) {
    const { scenario } = this.state;
    const saveFile = new SaveFile( arrayBuffer );
    scenario.globals[ GLOBAL.PLAYER_NAME ] = saveFile.player_name;
    debugger;
  }

  async print (str) {
    if (this.dialogBuffer) {
      const { keywords=[] } = this.dialogBuffer;
      (str.match( /(@[a-zA-Z]+)/g ) || []).forEach(
        key => keywords.unshift( key.substr(1,1).toUpperCase() + key.substr(2) ) );
      this.dialogBuffer.text = (this.dialogBuffer.text || '') + str;
      this.dialogBuffer.keywords = keywords;
      console.log( 'dialogBuffer', this.dialogBuffer );
    } else {
      this.setState(
        { logText: this.state.logText + str },
        () => {
          const log = document.querySelector('#hud .log');
          if (log) {
            log.scrollTop = log.scrollHeight;
          }
        } );
    }
  }

  renderLandscapeInfo () {
    const YEAR = 365.25 * 24;
    const DAY = 24 * YEAR / (YEAR + 24);
    const { daySlider, timeSlider } = this.state;
    const hour = Math.floor( timeSlider );
    const minute = 60 * (timeSlider - Math.floor( timeSlider ));
    return H.div(
      {},
      H.label(
        {},
        `Day ${String(1 + Math.floor( daySlider / DAY )).padStart(3,'0')}`,
        H.input({
          type: 'range',
          min: 0,
          max: 8736,
          step: 24,
          value: daySlider,
          onChange: e => this.setState({ daySlider: parseFloat( e.currentTarget.value ) }) }) ),
      H.br(),
      H.label(
        {},
        `${String(hour).padStart(2,'0')}:${String(minute).padStart(2,'0')}`,
        H.input({
          type: 'range',
          min: 0,
          max: 23.75,
          step: 0.25,
          value: timeSlider,
          onChange: e => this.setState({ timeSlider: parseFloat( e.currentTarget.value ) }) }) ) );
  }

  async newGame (skipIntro) {

    const { scenario, currentZone } = this.state;

    const name = skipIntro ? 'Bellerophon' : await new Promise( resolve => {
      const { windows, scenario } = this.state;
      windows.push({
        key: 'player-name',
        type: PlayerNameWindow,
        top: (document.body.clientHeight / 2) - 64,
        left: (document.body.clientWidth / 2) - 256,
        close: resolve,
      });
      this.setState({ windows });
    } );

    this.setState({ windows: this.state.windows.filter( w => w.key !== 'player-name' ) });

    if (!name) { return; }

    const res = skipIntro ? { gender: 0, archetype: 0, portrait: 0 } : await new Promise( resolve => {
      const { windows, scenario } = this.state;
      windows.push({
        key: 'new-character',
        type: NewCharacterWindow,
        top: (document.body.clientHeight / 2) - 217,
        left: (document.body.clientWidth / 2) - 208,
        scenario,
        close: resolve,
      });
      this.setState({ windows });
    } );

    const windows = this.state.windows.filter( w => w.key !== 'new-character' && w.key !== 'main-menu' );

    windows.push({
      key: 'world-window',
      type: WorldWindow,
      left: 16,
      top: 16,
      width: 480,
      height: 480,
      scenario,
    });

    if (!res) { return; }

    scenario.globals[ GLOBAL.PLAYER_NAME ] = name;
    const pc = scenario.globals[ GLOBAL.PLAYER_CHARACTER ] = scenario.characters[1];
    await pc.init( res.gender, res.archetype );
    // TODO find what actually sets the portrait and use/inventory space
    // skills get set when first talking with alaric
    // pc.skills = scenario.getResource( 0x501 ).get(pc.archetype).slice(3);
    pc._portrait = scenario.portraits[ (res.gender ? 0xf5 : 0xef) + res.portrait ];
    this.changeZone(1);

    this.setState(
      { windows, showHud: true, party: [pc.id] },
      () => this.print( '\nWelcome to Delver v1.0.3\n\nEnjoy your stay!\n' ) );

    this.redraw();
  }

  redraw () {
    this.setState({ redraw: ++this.state.redraw });
  }

  register (to) {
    const { scenario, windows } = this.state;
    const mainMenu = windows.find( ({ key }) => key === 'main-menu' );
    if (mainMenu) {
      mainMenu.registered = to || null;
      this.setState({ windows });
    }
    scenario.globals[ GLOBAL.REGISTERED ] = !!to;
  }

  render () {
    const {
      scenario, showHud, party, slideshow,
      loaded, viewing, windows, dialog, logText,
      action, quests, lookingAt, cursor,
      cameraX, cameraY, currentZone,
      drawPropsCheck, drawFauxPropsCheck, drawDebugPropsCheck, drawRoofsCheck
    } = this.state;

    return [

      slideshow
        ? H.div(
          { className: 'slideshow', onClick: () => slideshow.resume(false) },
          H.img({ src: slideshow.image === 0x201 ? '/img/intro.png' : '' }),
          H.p({ className: 'argos' }, slideshow.text) )
        : null,

      windows.map(
        ({ type, ...props },i) =>
          c( type,
             {
               lookAt: (lookingAt,x=0,y=0) => {
                 this.setState({ lookingAt, cursor: { x, y } });
               },
               setState: state => {
                 const { windows } = this.state;
                 Object.assign( windows[i], state );
                 this.setState({ windows });
               },
               ...props,
               ...(type === WorldWindow || type === DebugWindow
                   ? { cameraX, cameraY, currentZone, cursor,
                       redraw: this.state.redraw, title: this.state.title,
                       click: (x,y) => this[this.state.action]({ x, y }),
                       drawPropsCheck, drawFauxPropsCheck, drawDebugPropsCheck, drawRoofsCheck }
                   : {})
             } ) ),

      showHud
        ? c( Hud, { key: 'hud', scenario, logText, action, quests, lookingAt, party } )
        : null,

      dialog
        ? c( Dialog,
             { key: 'dialog',
               ...dialog,
               scenario,
               lookAt: lookingAt => this.setState({ lookingAt }) } )
        : null

    ];

  }

  async changeZone (zoneport) {
    const { scenario, currentZone } = this.state;
    const { zone, x, y } = scenario.zoneports[ zoneport ];
    const pc = scenario.globals[ GLOBAL.PLAYER_CHARACTER ];
    scenario.zones[ currentZone ].removeProp( pc );
    scenario.zones[ zone ].addPropAt( pc, x, y );
    const hour = scenario.globals[ GLOBAL.CURRENT_HOUR ];
    for (let npc of scenario.zones[ zone ].npcs) {
      const tasks = npc.schedule
            .filter( s => s.zone === zone )
            .filter( (s,i,ar) => s.hour <= hour && (!ar[i+1] || ar[i+1].hour > hour) );
      const task = tasks[ Math.floor( Math.random() * tasks.length ) ];
      if (task) {
        console.log( `Placing ${npc.name} at (${task.x}, ${task.y})` );
        await npc.init();
        scenario.zones[ zone ].addPropAt( npc, task.x, task.y );
      }
    }
    scenario.globals[ GLOBAL.CURRENT_ZONE ] = zone;
    this.setState({ currentZone: zone, cameraX: x + 1, cameraY: y + 1 });
    console.log( 'running zone script' );
    scenario.zone_scripts[ zone ].table.get(0x14).call(0);
    this.changeRoom( scenario.zones[ zone ].getRoom( x, y ) );
    this.redraw();
  }

  async changeRoom (room) {
    const { scenario } = this.state;
    const lastRoom = scenario.globals[ GLOBAL.CURRENT_ROOM ];
    const currentRoom = scenario.globals[ GLOBAL.CURRENT_ROOM ] = room ? room.prop_id : 0;
    if (lastRoom !== currentRoom && !scenario.globals.visited[ currentRoom ]) {
      scenario.globals.visited[ currentRoom ] = true;
      const look = scenario.getResource( 0x1b00 + currentRoom ).get( 0x07 );
      if (look) {
        await look.call( room );
      }
    }
  }

  findObject ({ subject, object, dir, x, y }) {
    const { scenario, currentZone } = this.state;

    if (!object) {

      if (typeof dir !== 'undefined') {
        if (subject) {
          subject.aspect = dir * 4;
        }

        switch (dir) {
        case 0: --y; break;
        case 1: ++x; break;
        case 2: ++y; break;
        case 3: --x; break;
        }

      }

      const zone = scenario.zones[ currentZone ];

      object = zone.getProps( x, y )
        .filter( p => p.visible )
        .find( p => scenario.getResource( 0x1000 | p.prop_id ) );

    }

    return object;
  }

  async move ({ subject=scenario.characters[1],
                centerCamera,
                dir,
                x=subject.x,
                y=subject.y })
  {
    const { scenario, currentZone } = this.state;

    if (typeof dir !== 'undefined') {
      subject.aspect = dir * 4;

      switch (dir) {
      case 0: --y; break;
      case 1: ++x; break;
      case 2: ++y; break;
      case 3: --x; break;
      }

    }

    const zone = scenario.zones[ currentZone ];

    const impassable = prop => prop.attributes & 0x200;
    const explicitlyPassable = prop => (prop.attributes & 0xc00000) === 0xc00000; // for the magic wall in lkh

    const youShallNotPass = (impassable( zone.getTile( x, y ) )
                             || zone.getProps( x, y ).some( prop => impassable( prop ) ))
          && !zone.getProps( x, y ).some( prop => explicitlyPassable( prop ) );

    if (!youShallNotPass) {
      if (y < 0) {
        this.changeZone( zone.exit_zoneport_north );
      } else if (x >= zone.width) {
        this.changeZone( zone.exit_zoneport_east );
      } else if (y >= zone.height) {
        this.changeZone( zone.exit_zoneport_south );
      } else if (x < 0) {
        this.changeZone( zone.exit_zoneport_west );
      } else {
        zone.movePropTo( subject, x, y );
        this.changeRoom( zone.getRoom( x, y ) );
        if (centerCamera) {
          this.setState({ cameraX: x + 1, cameraY: y + 1 });
        }
        // scenario.globals[ GLOBAL.CURRENT_TIME ] += 11;
      }
    }
    this.redraw();
  }

  async use ({ subject=scenario.characters[1],
               object,
               indirect_object,
               dir,
               x=subject.x,
               y=subject.y })
  {

    const { scenario } = this.state;

    object = this.findObject({ subject, object, dir, x, y });

    if (object) {

      const use = scenario.getResource( 0x1000 | object.prop_id ).get( 0x09 );

      this.print( `> Use ${object.prop ? object.prop.name : object.name}.\n` );

      if (use) {
        console.log( await use.toString() );
        await use.call( object );
        this.redraw();
      }

    }
    this.setState({ action: 'move' });
  }

  async look ({ subject=scenario.characters[1],
                object,
                dir,
                x=subject.x,
                y=subject.y })
  {

    const { scenario } = this.state;

    object = this.findObject({ subject, object, dir, x, y });

    if (object) {

      const examine = scenario.getResource( 0x1000 | object.prop_id ).get( 0x08 );

      if (examine) {
        await examine.call( object );
      } else {
        const article = object.attributes & 0x800000 ? 'an '
              : object.attributes & 0x400000 ? 'a '
              : '';
        this.print( `> Look - you see ${article}${object.name}.\n` );
      }

    }
    this.setState({ action: 'move' });
  }

  async talk ({ subject=scenario.characters[1],
          object,
          dir,
          x=subject.x,
          y=subject.y })
  {

    const { scenario } = this.state;

    object = this.findObject({ subject, object, dir, x, y });

    if (object) {

      scenario.globals.emit( 'open-conversation' );
      scenario.globals.emit( 'talk-participant', { slot: 2, participant: subject.id } );
      scenario.globals.emit( 'talk-participant', { slot: 0, participant: object.id } );

      const talk = scenario.data[23][object.id].get(12);

      if (talk) {
        await talk.call( subject.id );
      }

      if (this.dialogBuffer) {
        this.setState({ dialog: {
          ...this.dialogBuffer,
          expectResponse: false,
          willFinish: true,
          resume: () => this.setState({ dialog: null })
        } });
        this.dialogBuffer = null;
      }

    }

    this.setState({ action: 'move' });
  }

  async lookAround () {
    const { scenario, currentZone } = this.state;
    const room = scenario.zones[ currentZone ].rooms.find( room => room.prop_id === scenario.globals[ GLOBAL.CURRENT_ROOM ] );
    if (room) {
      const look = scenario.getResource( 0x1b00 + room.prop_id ).get( 0x07 );
      if (look) {
        this.print( '> Look Around.\n' );
        await look.call( room );
      }
    }

  }

}

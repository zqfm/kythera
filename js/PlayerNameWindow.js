const H = ReactDOMFactories;
const { createElement: c, useState } = React;

import Window from './Window.js';

export default function PlayerNameWindow (props) {
  const [name,setName] = useState('Bellerophon');

  return c(
    Window,
    { ...props,
      className: 'player-name alt',
      width: 480,
      height: 96 },
    H.label(
      { className: 'argos', style: { position: 'absolute', top: '16px', left: '16px' } },
      'Enter your name: ',
      H.input({
        type: 'text',
        value: name,
        maxLength: 96,
        onChange: e => setName( e.currentTarget.value )
      }) ),
    H.div(
      { style: { position: 'absolute', bottom: '16px', right: '16px' } },
      H.button(
        { className: 'button',
          onClick: e => props.close(null) },
        'Cancel' ),
      H.button(
        { className: 'button',
          style: { marginLeft: '8px' },
          onClick: e => props.close(name) },
        'Ok' ) )
  );

}

import { GLOBAL } from './delv/constants.js';
import Prop from './delv/Prop.js';
import Character from './delv/Character.js';

export async function test (scenario) {

  console.group( 'testing scenario...' );

  // header information
  console.assert( scenario.title === 'Cythera: Fate of Alaric',
          `Unexpected scenario title: ${scenario.title}` );

  console.assert( scenario.master_index_offset === 0x80,
          `Unexpected master index offset: ${scenario.master_index_offset}` );

  console.assert( scenario.master_index_size === 0x800,
          `Unexpected master index length: ${scenario.master_index_length}` );

  // random master index sample

  console.assert( scenario.data[0].byteOffset === 5526054,
          `Incorrect offset, expecting 5526054 got ${scenario.data[0].offset}` );

  console.assert( scenario.data[0].byteLength === 2048,
          `Incorrect length, expecting 2048 got ${scenario.data[0].length}` );

  console.assert( scenario.data[1][1].resource.getUint8(0) === 0x91,
                  `Expected decrypted value to be 145, got ${scenario.data[1][1].resource.getUint8(0)}` );

  console.assert( scenario.data[1][1].length === 256,
                  `Expected character name list to have 256 elements, instead got ${scenario.data[1][1].length}` );

  // character creation

  let pc;
  for (let i = 0; i < 1/*8*/; ++i) {

    pc = scenario.globals[ GLOBAL.PLAYER_CHARACTER ] = new Character( scenario );

    await scenario.data[23][1].table.get(0).call( pc, 0, i );

    switch (i) {
    case 0:
      console.assert( pc.body === 16, 'explorer should have 16 body' );
      console.assert( pc.reflex === 16, 'explorer should have 16 reflex' );
      console.assert( pc.mind === 16, 'explorer should have 16 mind' );
      console.assert( pc.full_health === 25, 'explorer should start with 25 health' );
      console.assert( pc.full_mana === 0, 'explorer should start with 0 mana' );
      console.assert( pc.use === 16, 'explorer should start with 16 use space' );
      console.assert( pc.inv === 32, 'explorer should start with 32 inventory space' );
      break;
    case 1:
      console.assert( pc.body === 18, 'fighter should have 18 body' );
      console.assert( pc.reflex === 18, 'fighter should have 18 reflex' );
      console.assert( pc.mind === 12, 'fighter should have 12 mind' );
      console.assert( pc.full_health === 28, 'fighter should start with 28 health' );
      console.assert( pc.full_mana === 0, 'fighter should start with 0 mana' );
      console.assert( pc.use === 18, 'fighter should start with 18 use space' );
      console.assert( pc.inv === 36, 'fighter should start with 36 inventory space' );
      break;
    case 2:
      console.assert( pc.body === 17, 'swordsman should have 17 body' );
      console.assert( pc.reflex === 19, 'swordsman should have 19 reflex' );
      console.assert( pc.mind === 12, 'swordsman should have 12 mind' );
      console.assert( pc.full_health === 27, 'swordsman should start with 27 health' );
      console.assert( pc.full_mana === 0, 'swordsman should start with 0 mana' );
      console.assert( pc.use === 17, 'swordsman should start with 17 use space' );
      console.assert( pc.inv === 34, 'swordsman should start with 34 inventory space' );
      break;
    case 3:
      console.assert( pc.body === 20, 'berserker should have 20 body' );
      console.assert( pc.reflex === 16, 'berserker should have 16 reflex' );
      console.assert( pc.mind === 12, 'berserker should have 12 mind' );
      console.assert( pc.full_health === 29, 'berserker should start with 29 health' );
      console.assert( pc.full_mana === 0, 'berserker should start with 0 mana' );
      console.assert( pc.use === 20, 'berserker should start with 20 use space' );
      console.assert( pc.inv === 40, 'berserker should start with 40 inventory space' );
      break;
    case 4:
      console.assert( pc.body === 10, 'mage should have 10 body' );
      console.assert( pc.reflex === 18, 'mage should have 18 reflex' );
      console.assert( pc.mind === 20, 'mage should have 20 mind' );
      break;
    case 5:
      console.assert( pc.body === 10, 'wizard should have 10 body' );
      console.assert( pc.reflex === 18, 'wizard should have 18 reflex' );
      console.assert( pc.mind === 20, 'wizard should have 20 mind' );
      break;
    case 6:
      console.assert( pc.body === 10, 'mystic should have 10 body' );
      console.assert( pc.reflex === 18, 'mystic should have 18 reflex' );
      console.assert( pc.mind === 20, 'mystic should have 20 mind' );
      break;
    case 7:
      console.assert( pc.body === 16, 'storyteller should have 16 body' );
      console.assert( pc.reflex === 18, 'storyteller should have 18 reflex' );
      console.assert( pc.mind === 14, 'storyteller should have 14 mind' );
      break;
    case 8:
      console.assert( pc.body === 16, 'rogue should have 16 body' );
      console.assert( pc.reflex === 18, 'rogue should have 18 reflex' );
      console.assert( pc.mind === 14, 'rogue should have 14 mind' );
      break;
    }

  }

  // some scripts

  pc.nutrition = 0;

  const potionScript = scenario.data[9][0];

  console.log( 'drinking potion...' );

  await potionScript.call( pc );

  console.assert( pc.nutrition === 24 );

  // dialog

  /** /
  console.log( 'open dialog' );

  scenario.globals.emit( 'open-conversation' );
  scenario.globals.emit( 'talk-participant', { slot: 2, participant: 1 } );
  scenario.globals.emit( 'talk-participant', { slot: 0, participant: 2 } );

  scenario.globals.state.set( 0, 0 );
  pc.bit_flags = 0;

  await scenario.data[23][2].get(12).call(
    scenario.globals[5],
    //Object.assign( new Prop(scenario, { id: 0, flags: 66, aspect: 9 }), { obj_type: 0 } )
  );
  /**/

  console.groupEnd();

  return true;

}

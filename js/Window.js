const H = ReactDOMFactories;
const { useState } = React;

// dirty hack, this should be handled by react
let highZ = 1000;

export default function Window (props) {

  const { className, title, canResize, canClose, close, style={}, children } = props;

  const [left,setLeft] = useState( props.left || 0 );
  const [top,setTop] = useState( props.top || 0 );
  const [width,setWidth] = useState( props.width );
  const [height,setHeight] = useState( props.height );

  return H.div(
    { key: className,
      className: `window ${className}`,
      style: { left, top, width, height },
      onMouseDown: e => {
        const clickedBorder = e.pageX - left > width + 16
              || e.pageX - left < 16
              || e.pageY - top > height + 16
              || e.pageY - top < 16;
        if (e.button === 0 && clickedBorder) {
          const z = e.currentTarget.style.zIndex;
          if ((!z || z < highZ) && !props.noZ) {
            e.currentTarget.style.zIndex = highZ++;
          }
          e.preventDefault();
          const offsetX = e.pageX - left;
          const offsetY = e.pageY - top;
          const drag = e => {
            e.preventDefault();
            setLeft( e.pageX - offsetX );
            setTop( e.pageY - offsetY );
          };
          document.addEventListener( 'mousemove', drag );
          document.addEventListener( 'mouseup', () => {
            document.removeEventListener( 'mousemove', drag );
          }, { once: true } );
        }
      } },
    title ? H.header({}, H.h1({}, H.span({}, title ))) : null,
    canClose ? H.button({
      className: 'close',
      onClick: e => close(),
    }) : null,
    H.div({ className: 'content', style }, children),
    canResize ? H.button({
      className: 'resize',
      onMouseDown: e => {
        if (e.button === 0) {
          e.preventDefault();
          e.stopPropagation();
          const offsetX = e.pageX - (left + width);
          const offsetY = e.pageY - (top + height);
          const drag = e => {
            e.preventDefault();
            const w = Math.max( Math.floor( (e.pageX - left - offsetX) / 32 ) * 32, 160 );
            const h = Math.max( Math.floor( (e.pageY - top - offsetY) / 32 ) * 32, 160 );
            if (props.onResize && (w !== width || h !== height)) {
              props.onResize( w, h );
            }
            setWidth( w );
            setHeight( h );
          };
          document.addEventListener( 'mousemove', drag );
          document.addEventListener( 'mouseup', () => {
            document.removeEventListener( 'mousemove', drag );
          }, { once: true } );
        }
      }
    }) : null );

}

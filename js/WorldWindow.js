const H = ReactDOMFactories;
const { createElement: c, useEffect, useRef, useState } = React;

import { hex } from './delv/Resource.js';
import Tile from './delv/Tile.js';
import Window from './Window.js';

function pluralName (prop) {
  if (/\\/.test( prop.name )) {
    const [pfx,sfx] = prop.name.split(/\\/);
    const one = prop.d3 === 1;
    if (/\//.test( sfx )) {
      const [sing,plur] = sfx.split(/\//);
      return pfx + (one ? sing : plur);
    } else {
      return pfx + (one ? '' : sfx);
    }
  } else {
    return prop.name;
  }

}

export default function WorldWindow (props) {
  const {
    scenario,
    cameraX,
    cameraY,
    drawPropsCheck,
    drawFauxPropsCheck,
    drawDebugPropsCheck,
    drawRoofsCheck,
    currentZone,
    redraw,
    title,
  } = props;

  const [[width,height],setDims] = useState([props.width,props.height]);
  const [[tx,ty],setHover] = useState([0,0]);

  const canvas = useRef(null);

  const drawZone = () => {
    const zone = scenario.zones[ currentZone ];
    if (zone) {
      const ctx = canvas.current.getContext('2d');
      ctx.clearRect( 0, 0, width, height );
      zone.draw(
        ctx, cameraX, cameraY,
        { width: Math.ceil( width / 32 ),
          height: Math.ceil( height / 32 ),
          hideProps: !drawPropsCheck,
          hideFauxProps: !drawFauxPropsCheck,
          hideDebugProps: !drawDebugPropsCheck } );
    }
  };

  const topProp = (zone, tx, ty, withHidden=false) => {
    const propList = zone.getProps( tx, ty )
          .filter( withHidden ? p => true : p => p.visible )
          .sort( (a,b) => {
            const byattr = Tile.draw_priority(a.attributes) - Tile.draw_priority(b.attributes);
            if (byattr) {
              return byattr;
            } else {
              return b.tile_id - a.tile_id;
            }
          } );

    return propList.length > 0 ? propList[ propList.length - 1 ] : null;
  };

  useEffect( () => drawZone(), [
    currentZone, cameraX, cameraY, width, height, redraw,
    drawPropsCheck, drawFauxPropsCheck, drawDebugPropsCheck, drawRoofsCheck
  ] );

  useEffect( () => {
    const zone = scenario.zones[ currentZone ];
    const top = topProp( zone, tx, ty );

    props.lookAt(
      top ? `${pluralName( top )} (0x${hex(top.attributes)})` : zone.getTile( tx, ty ).name,
      tx,
      ty );
  }, [tx,ty] );

  return c(
    Window,
    { ...props, title, canResize: true, className: 'world', onResize: (w,h) => setDims([w,h]) },
    H.canvas({
      ref: canvas,
      width,
      height,
      onMouseDown: e => {
        e.stopPropagation();
        props.click( tx, ty );
      },
      onMouseMove: e => {
        const rect = e.currentTarget.getBoundingClientRect();
        const x = e.pageX - rect.left;
        const y = e.pageY - rect.top;
        setHover([
          Math.floor( x / 32 ) + cameraX - Math.ceil( width / 64 ),
          Math.floor( y / 32 ) + cameraY - Math.ceil( height / 64 )
        ]);
      },
      onMouseUp: e => {
        e.stopPropagation();
      }
    }) );

}

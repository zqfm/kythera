const base = 36;

export default function randomId (len=9) {
  let id = '';
  for (let i = 0; i < len; ++i) {
    id += Math.floor(Math.random() * base).toString(base);
  }
  return id;
}

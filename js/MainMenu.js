const H = ReactDOMFactories;
const { createElement: c, useEffect, useRef, useState } = React;

import Window from './Window.js';

const btnDn = new Audio('/snd/Button_Down.wav');
const btnUp = new Audio('/snd/Button_Up.wav');

export default function MainMenu (props) {

  const [frame,setFrame] = useState(1);

  useEffect( () => {
    const timeout = setTimeout( () => {
      setFrame( frame < 6 ? frame + 1 : 1 );
    }, 100 );
    return () => clearTimeout( timeout );
  }, [frame] );

  const button = (opts, ...children) =>
        H.button(
          {
            onMouseDown: e => {
              btnDn.play();
              document.addEventListener( 'mouseup', e => btnUp.play(), { once: true } );
            },
            ...opts,
          },
          ...children );

  return c(
    Window,
    { ...props,
      width: 640,
      height: 480,
      noZ: true, // don't raise above other windows
      className: 'main-menu alt' },
    H.div({ className: `flame flame-${frame}` }),
    H.span({ className: 'save-name argos no-player' }, 'No Player Selected'),
    H.span({ className: 'registration argos' }, props.registered ? `Registered To: ${props.registered}` : 'Unregistered'),
    button({ className: 'onward', disabled: true }, 'Onward'),
    button({ className: 'preferences' }, 'Preferences'),
    button({ className: 'new-game', onClick: e => props.newGame( e.shiftKey ) }, 'New Game'),
    button({ className: 'about' }, 'About'),
    button({ className: 'open-game',
               onClick: () => {
                 const input = document.createElement('input');
                 input.type = 'file';
                 input.hidden = true;
                 document.body.appendChild( input );
                 input.onchange = async (e) => {

                   const file = e.currentTarget.files[0];
                   const arrayBuffer = await new Promise( resolve => {
                     const fr = new FileReader();
                     fr.onload = e => resolve( e.target.result );
                     fr.readAsArrayBuffer( file );
                   } );

                   props.onSaveFileLoaded( arrayBuffer );

                 };
                 input.click();
                 document.body.removeChild( input );
               } }, 'Open Game'),
    button({ className: 'quit', onClick: () => window.close() }, 'Quit')
  );
}

const H = ReactDOMFactories;
const { createElement: c, useEffect, useState, useRef } = React;
import { hex } from './delv/Resource.js';

import Tile from './delv/Tile.js';
import Window from './Window.js';

export default function DebugWindow (props) {
  const { scenario } = props;

  const [viewing,setViewing] = useState(null);
  const [zoneNames,setZoneNames] = useState([]);
  const [portraitPage,setPortraitPage] = useState(0);
  const [tileset,setTileset] = useState(0);
  const [zoneSelect,setZoneSelect] = useState(props.currentZone);

  const canvas = useRef(null);

  const loadZoneNames = async () => {
    if (scenario) {
      const names = [];
      const zoneports = [...Array( 256 ).keys()].map( i => scenario.zoneports[i] );
      for (let i = 0; i < 42; ++i) {
        const zone = scenario.zone_scripts[i];
        const zoneport = zoneports.findIndex( ({ zone }) => zone === i );
        const script = zone.table.get(0x14);
        const line = (await script.body).find( line => line[1] === 'sys' && line[2] === 'set-title' );
        if (line) {
          names.push({ name: line[3][1], resid: hex( zone.resource.resid, 4 ), zoneport });
        }
      }
      setZoneNames(names);
    }
  };

  const drawTileset = () => {
    const ctx = canvas.current.getContext('2d');
    ctx.clearRect( 0, 0, 512, 512 );
    for (let i = 0; i < 16; ++i) {
      const sheet = scenario.tilesheets[ i + (tileset*4) ];
      if (sheet) {
        for (let j = 0; j < 16; ++j) {
          sheet.drawTile( ctx, j, 32 * Math.floor( (j + i*16) / 16 ), 32 * ((j + i*16) % 16) );
        }
      }
    }
  };

  const drawPortraits = () => {
    const ctx = canvas.current.getContext('2d');
    ctx.clearRect( 0, 0, 512, 512 );
    for (let i = 0; i < 64; ++i) {
      const j = i + (portraitPage * 16);
      const portrait = scenario.portraits[j];
      if (portrait) {
        portrait.draw( ctx, 64 * Math.floor( i / 8 ), 64 * (i % 8) );
      }
    }
  };

  const drawSkillIcons = () => {
    const ctx = canvas.current.getContext('2d');
    ctx.clearRect( 0, 0, 512, 512 );
    for (let i = 0; i < 96; ++i) {
      const skill_icon = scenario.skill_icons[i];
      if (skill_icon) {
        skill_icon.draw( ctx, 32 * Math.floor(i / 32), 16 * ( i % 32 ) );
      }
    }
  };

  useEffect( () => { loadZoneNames(); }, [scenario] );

  useEffect( () => {
    switch (viewing) {
    case 'tileset': drawTileset(); break;
    case 'portraits': drawPortraits(); break;
    case 'skill-icons': drawSkillIcons(); break;
    }
  }, [viewing,tileset,portraitPage] );

  const renderZoneInfo = () => {

    const { cursor: { x, y }, cameraX, cameraY } = props;
    const zone = scenario.zones[ zoneSelect ];
    if (!zone) { return null; }
    const tile = zone.getTile( x, y );
    const propList = zone.getProps( x, y );
    const fp = tile.faux_prop;
    const fp_info = fp
          ? `${fp.name} (0x${hex(fp.prop_id)}) tile_id 0x${hex(fp.tile_id)} aspect 0x${hex(fp.aspect)} rotate ${fp.rotate} offset [${fp.offset.join(',')}] tile 0x${hex(fp.tile_id)} attr 0x${hex(fp.attributes)}`
          : 'N/A';

    return H.div(
      {},
      H.label( {}, H.input({
        type: 'checkbox',
        checked: props.drawPropsCheck,
        onChange: e => props.setState({ drawPropsCheck: e.currentTarget.checked } )
      }), H.span({}, 'Draw Props') ),
      H.label( {}, H.input({
        type: 'checkbox',
        checked: props.drawFauxPropsCheck,
        onChange: e => props.setState({ drawFauxPropsCheck: e.currentTarget.checked } )
      }), H.span({}, 'Draw Faux Props') ),
      H.label( {}, H.input({
          type: 'checkbox',
          checked: props.drawDebugPropsCheck,
          onChange: e => props.setState({ drawDebugPropsCheck: e.currentTarget.checked } )
        }), H.span({}, 'Draw Debug Props') ),
      H.label( {}, H.input({
        type: 'checkbox',
        checked: props.drawRoofsCheck,
        onChange: e => props.setState({ drawRoofsCheck: e.currentTarget.checked } )
      }), H.span({}, 'Draw Roofs') ),
      H.br(),
      H.label( {}, `Cursor: (${x},${y})` ),
      H.br(),
      H.label( {}, `Tile: ${tile && tile.name || ''} (0x${tile && hex(tile.tile_id) || '0'}) attr 0x${hex(tile.attributes)}` ),
      H.br(),
      H.label( {}, 'Faux Prop: ' + fp_info ),
      H.br(),
      H.table(
        { },
        H.thead(
          {},
          H.tr(
            {},
            H.th({}, 'Prop'),
            H.th({}, 'Tile'),
            H.th({}, 'Flags'),
            H.th({}, 'Location'),
            H.th({}, 'Attr'),
            H.th({}, 'Aspect'),
            H.th({}, 'Rotate'),
            H.th({}, 'D3'),
            H.th({}, 'PropRef'),
            H.th({}, 'StoreRef'),
            H.th({}, '?') ) ),
        H.tbody(
          {},
          propList.map( entry => entry && typeof entry.flags !== 'undefined' ? H.tr(
            { key: entry.index },
            H.td({}, `${entry.name} (0x${hex(entry.prop_id)})`),
            H.td({}, '0x' + hex(entry.tile_id + entry.aspect)),
            H.td({}, '0x' + hex(entry.flags)),
            H.td({}, `(${entry.x},${entry.y}) 0x${hex(entry.raw_location)}`),
            H.td({}, '0x' + hex(entry.attributes)),
            H.td({}, entry.aspect),
            H.td({}, entry.rotate),
            H.td({}, '0x' + hex(entry.d3)),
            H.td({}, '0x' + hex(entry.propref)),
            H.td({}, '0x' + hex(entry.storeref)),
            H.td({}, '0x' + hex(entry.u)),
          ) : null ) ) ) );

  };

  return c(
    Window,
    { ...props,
      title: 'Debug',
      className: 'debug-window alt',
      canResize: true,
      canClose: true,
      style: { padding: '16px', fontSize: '8pt' },
      width: 544,
      height: 704 },
    H.div(
      { className: 'mini-border',
        style: { height: '144px', overflowY: 'scroll' } },
      renderZoneInfo(),
      H.input( { type: 'number', value: portraitPage,  min: 0, max: 12, onChange: e => {
        const portraitPage = parseInt( e.currentTarget.value, 10 );
        if (!isNaN(portraitPage)) {
          setViewing('portraits');
          setPortraitPage(portraitPage);
        }
      } } ),
      H.button({ className: 'button', type: 'button', onClick: e => setViewing('portraits') }, 'Draw Portraits'),
      H.br(), H.br(),
      H.button({ className: 'button', type: 'button', onClick: e => setViewing('skill-icons') }, 'Draw Skill Icons'),
      H.br(), H.br(),
      H.input( { type: 'number', value: tileset,  min: 0, max: 36, onChange: e => {
        const tileset = parseInt( e.currentTarget.value, 10 );
        if (!isNaN(tileset)) {
          setViewing('tileset');
          setTileset(tileset);
        }
      } } ),
      H.button({ className: 'button', type: 'button', onClick: e => setViewing('tileset') }, 'Draw Tileset'),
      H.br(), H.br(),
      H.select(
        { value: zoneSelect,
          onChange: e => {
            const zoneSelect = e.currentTarget.value;
            if (!isNaN(zoneSelect)) {
              setZoneSelect(zoneSelect);
              props.changeZone(zoneSelect);
            }
          } },
        zoneNames.map( ({name,resid,zoneport},i) => H.option( { key: resid, value: zoneport }, `${name} (${resid})` ) ) ) ),
    H.canvas({
      className: 'mini-border',
      width: 512,
      height: 512,
      ref: canvas,
      style: {
        width: '512px',
        height: '512px',
        background: 'black',
        marginTop: '16px'
      },
      onMouseMove: e => {
        const rect = e.currentTarget.getBoundingClientRect();
        const x = e.pageX - rect.left;
        const y = e.pageY - rect.top;
        if (viewing === 'portraits') {
          props.lookAt( scenario.data[1][1].get(
            (Math.floor(y / 64) % 8)
              + (Math.floor(x / 64) * 8)
              + (portraitPage * 16)
              + 1 ) );
        } else if (viewing === 'tileset') {
          const tileid = Math.floor( x / 32 ) * 16
                + Math.floor( y / 32 )
                + (tileset * 0x40);
          const tile = new Tile( scenario, tileid );
          props.lookAt( `${tile && tile.name || ''} (0x${hex(tile && tile.tile_id || 0)})` );
        }
      },
      onClick: e => {
        const rect = e.currentTarget.getBoundingClientRect();
        const x = e.pageX - rect.left;
        const y = e.pageY - rect.top;
        if (viewing === 'portraits') {
          props.clickPortrait(
            (Math.floor(y / 64) % 8)
              + (Math.floor(x / 64) * 8)
              + (portraitPage * 16)
              + 1 );
        }
      }
    })
  );

}

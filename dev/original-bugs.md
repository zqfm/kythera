

List of bugs in Cythera:

- NPCs' conversations are blank if you awaken them using Awakening.
- When the player sleeps, sometimes the NPCs in the zone disappear (until you leave the zone and return).
- A burned-out magical staff still produces light
- A light carried by a follower surrounds the player. (Maybe that's intentional, but it seems a little weird)
- Sometimes, all of the props disappear in LKH.
- Tlepolemus perpetually procrastinates when you ask to buy his pan-pipes.
- If you attempt to fill a pitcher with wine, it will say "the pitcher is now filled with wine" but the pitcher will still be empty.
- Darius and Sardis try to occupy the same chair at the Green Goat (ignoring the three empty chairs at that table), from 5pm through 9pm.
- When NPCs are killed in their sleep, their cadavers often turn into strange things.
- Closed doors cannot be opened if an item (such as a dagger thrown at the player earlier, when the door was open) is underneath.
- In the first undead stronghold, the kitchen door is a wall.
- Aethon ignores orders to pick locks.
- Using the spell "Fetch" on an item will only make the item disappear, it will not appear in the player's inventory.
- There is no deep water in the game, making it impossible to fish.
- The "Trap Detection & Removal" skill does not detect traps on containers, such as the poison traps on Halos' crates, or the blast traps on Deiphobus' chests.
- Training in gambling does not seem to increase the likelihood of winning oboloi in dice (maybe that's intentional, though).
- Stealing isn't wrong if you move the object first.
- Paris is always labelled as "man" instead of his name, even after learning it.
- Diomede is always labelled as "woman" instead of her name, even after learning it.
- In the [Narrations](http://people.ku.edu/~s499p797/cythera/narrations/), the "Death" narration sometimes does not contain the first paragraph (I'm not sure if it depends on the computer, the circumstances of the player's death, or what). Also, the "giving Pelagon the three shards" narration doesn't show up at all on some computers, it's just a plain black screen until you get to "you gave the only hope to the enemy, who killed you."
- When you ask Hector to wait or leave, then ask Hadrian about Hector, you appear to say a line that Hadrian should be saying.
- If, the first time you talk to Lindus, you decline to learn magic, then every time you tell him "bye" (even after learning magic) he will nag you to come back & learn magic.
- In the initial conversation with Niobe (with Helen on-screen), Helen interjects and for the rest of the conversation, Niobe's responses appear as Helen's responses.
- If Aethon's in your party and you ask him to leave, he will respond as though he's leaving, but does not leave the party.
- If you tell Ennomus (the drunk in Kosha) that you've never been to Cademia, he gets confused and responds as though you said you've been to Cademia, but aren't familiar with the castle there.
- Killing NPCs improves your karma instead of lowering it.
- Although Hector, Meleager, Timon, and Aethon can get "drunk" (a state of confusion that quickly wears off), the player, Ariadne, and Dryas do not.
- When you remind Alaric that Chrysothemis was his mother, he will say he remembers that she died in 201, and he can remember parts of his history again. But when you ask him "history" after that, he'll say he can't figure out why the number 201 is significant to him.
- Most NPCs do not display all of the "Where Is" dialogue options. In LandKing Hall, Odemia, and Pnyx, the last two options never seem to show up. In Kosha and Catamarca, there seems to be a limit of 9 options (so the last one might not show up) and in Cademia, there seems to be a limit of 18 options (so the last few don't show up).
- When an NPC tries to estimate how many steps away from things you are, as a response to a "where is" prompt, they're not very good at it (particularly asking followers where various cities are from the overworld).
- Followers may wander off or disappear if you ask them to wait, and leave them.
- The "retrieve books of wisdom" task doesn't check off your to-do list when you give Selinus the ten Sapphire Books.
- If you start a game as a male, then start another game as a female, and go back to the male-character game, NPCs will refer to you as "she," "ma'am," "lady," etc. (Or if you did it the other way around, they'll refer to you as a man.)
- You cannot use the mouse to drag your character if another character (such as a follower) is occupying the same space.
- In the north-west corner of every zone, there's an invisible dead guy called Nothing.
- Players, especially if using the rogue archtype, often end up with an edible strange device near the beginning of the game.


Some characters refer to themselves in the third person:

- Halos' response to "Halos" (pretty amusing when he's in jail, saying it's hard to say whether he killed Opheltius or not).
- Demodocus' response to "music"
- Eioneus' response to "Eioneus"
- Charax' response to "Charax"
- Alaric's response to "king" or "LandKing"
- Parium, Crito, & Apis's responses to their names (they should respond to their own names the same way they respond to "inn," but that only works if you put a space in front of their names.)
- Eurybates' response to "Eurybates" (he should respond the same way that he responds to "name," but again, it only works if you add a space.)
- Propontis' response to "Propontis"
- Milcom's response to "Milcom"
- Philinus' response to "Philinus"
- Helen's response to "Helen"


In conversations, certain lines of text will flash by too quickly to read:

- Demodocus' description (beginning of the conversation)
- Crito's first line when you talk to him after asking Hebe about him.
- Antenor's response when you inquire about the building & decline upon hearing the price.
- Myus' description (beginning of conversation) when you talk to him the first time (or any time before asking him his name).
- Pheres' response when he asks you for a favour, & you agree but say you're not familiar with harpies (after the quick flash of text, it reverts to the "refusing to do him a favour" response).
- Borus' response when you mention Glaucus (for the first time, if you've already talked to Glaucus).
- The wishing fountain's responses to specific wishes (coffee, three, tequila, & pony)
- Ennomus' response when you tell him you've been to Cademia, but you're not familiar with the castle there.
- Thersites' description (beginning of conversation) during the two possible initial greetings, and when returning his ring.
- Asking Timon, Sacas, Metopes, Berossus, Bryaxis, Itanos, Eioneus, Charax, Lindus, Pheres, Palaestra, or Selinus "history" - the response flashes, then reverts to the "anything not recognized" response (or in the case of the Pnyx mages, it reverts to the generic Pnyx "history" response)
- The first few lines of the initial conversation with Niobe (with Helen interrupting. Helen must be on screen, & you need to have already learned Helen's name)
- Crito's greeting after he asked you to ask Hebe about him.
- Polydamas' description (beginning of conversation).
- Ake asking you to meet her in the garden at night to discuss the kidnapping, if you know Persuasion, that line will speed ahead to your refusal.
- Bartenders' first line after you ask for dice instructions then agree to play.


And my list of dialogue bits that I can't find in the game (I suspect that most(?) of these are bugs):

- Telling Hadrian that Hector is dead.
- Timon thinking I'm not the one he was waiting for.
- Timon's modified "metic" response to reflect that he's met Seldane.
- Telling Sacas I found kesh on Eudoxus.
- Telling Thuria about Jhiaxus, there should be another version of it.
- Seqedher, Unhayt, and Uset responding to "corruption" the same way that they respond to "Crolna."
- Sabinate realizing he already gave me the mushroom.
- Charax mentioning the underlying field properties of the Sea, Time, and Change.
- Alaric saying he's bonded to the land again, when prompted.
- Magpie telling you to "talk to the master first."
- Magpie having alternative responses to "Three," "Bahoudin," "Jhiaxus," Jinrai," & "Sabinate."
- Lindus having slightly different alternative responses to "metics" & "Timon."
- Eteocles having an alternative response to "kesh."
- Halos saying it's not a good time to talk politics, stop by his office during working hours.
- Alaric's alternative (looks like post-game) response to "job."
- Alternative (more descriptive) generic Cademia response to "Bryaxis."
- Thoas saying "please come again" after "Farewell."


Buzzzzy:

- The inventory duplication glitch
- The chairs in the first stronghold glow and shoot lightning instead of the lich (I think this also happens in the harpy cave with stalagmites)
- Magpie's obtainable end table
- Paris's immobile and unusable desk
- Bottle of kesh in the ruffian encampment that's super cumbersome despite weighing 1 grain
- Magic arrow graphic
- The shield in the ruffian encampment that acts like a weapon
- Clicking the bottom right corner of a party member's status window makes it go completely blank
- Flour can be placed anywhere
- The "white room" in LKH


Pallas Athene:

- You can hand in the same Sapphire book multiple times (is this the same as the inventory duplication glitch?)
- Containers let you carry ridiculous weights

BreadWorldMercy453:

> The Sapphire cheat involves putting the books in a container before turning them in to Selinus. The books will still appear to be in the container until you close & reopen it, and it's often possible to throw them on the floor, pick them up, & turn them in again. The inventory duplication glitch is when everything in your inventory doubles (though it's not just your inventory, also your equipment, skills, & NPCs' equipment/inventories. Tycho Maudd figured out a method to induce the inventory duplication glitch, but it's also happened to players randomly.


Sources

- http://sfiera.net/~sfiera/asw/topic/133390/1.html

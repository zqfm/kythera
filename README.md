# Getting started

First, copy your **Cythera Data** file into the project root.

``` bash
cp Cythera\ Data kythera/
```

Start the server.

``` bash
cd kythera && python -m http.server 9999
```

Then navigate to http://localhost:9999 in your browser (it seems to work for me in Firefox and Chromium).

To display the debug window, open the javascript console and run the `debug()` function.

Don't expect anything to work, a ton of stuff is broken or not implemented yet.

# Invaluable resources

* [delvmod](https://github.com/BryceSchroeder/delvmod) - about 99% of this project is just piggybacked from Bryce Schroeder's delvmod and would've been much different if it weren't for his work

# Valuable Sources

* [Cythera Guides](cytheraguides.com)
* [Slayer's guide to Cythera](www.cytheraguides.com/archives/Slayer/cythera.htm)
* [Cythera moons](sfiera.net/~sfiera/selenes.html)
